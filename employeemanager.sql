/*
SQLyog Ultimate v9.63 
MySQL - 5.6.17 : Database - employeemanager
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`employeemanager` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `employeemanager`;

/*Table structure for table `employee` */

DROP TABLE IF EXISTS `employee`;

CREATE TABLE `employee` (
  `employee_id` int(11) NOT NULL AUTO_INCREMENT,
  `packageprice_Id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `name` varchar(56) DEFAULT NULL,
  `mac_address` varchar(56) DEFAULT NULL,
  `address` text,
  `cnic` varchar(15) DEFAULT NULL,
  `phone` varchar(14) DEFAULT '-',
  `balance` double(11,2) DEFAULT '0.00',
  `monthly_amount` double(11,2) DEFAULT '0.00',
  `status` varchar(56) DEFAULT NULL,
  `cnicFront_url` varchar(125) DEFAULT NULL,
  `cnicBack_url` varchar(125) DEFAULT NULL,
  `image_url` varchar(125) DEFAULT NULL,
  `isActive` int(2) DEFAULT NULL,
  `join_date` date DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `is_paid` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`employee_id`),
  KEY `FK_employee` (`packageprice_Id`),
  KEY `location_id` (`location_id`),
  CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `location` (`location_id`),
  CONSTRAINT `FK_employee` FOREIGN KEY (`packageprice_Id`) REFERENCES `package_price` (`packageprice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;

/*Data for the table `employee` */

/*Table structure for table `location` */

DROP TABLE IF EXISTS `location`;

CREATE TABLE `location` (
  `location_id` int(11) NOT NULL AUTO_INCREMENT,
  `location_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `location` */

insert  into `location`(`location_id`,`location_name`) values (1,'Location3'),(2,'Location2'),(3,'Location1');

/*Table structure for table `monthly_payment` */

DROP TABLE IF EXISTS `monthly_payment`;

CREATE TABLE `monthly_payment` (
  `monthlyPayment_id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `paymentgenerate_Id` int(11) DEFAULT NULL,
  `paymentRate` double(12,2) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`monthlyPayment_id`),
  KEY `FK_monthly_payment` (`employee_id`),
  KEY `FK_monthly_payment2` (`paymentgenerate_Id`),
  CONSTRAINT `FK_monthly_payment` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`),
  CONSTRAINT `FK_monthly_payment2` FOREIGN KEY (`paymentgenerate_Id`) REFERENCES `paymentgenerate` (`paymentgenerate_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

/*Data for the table `monthly_payment` */

/*Table structure for table `package_price` */

DROP TABLE IF EXISTS `package_price`;

CREATE TABLE `package_price` (
  `packageprice_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_name` varchar(125) DEFAULT NULL,
  `package_price` double(11,2) DEFAULT NULL,
  PRIMARY KEY (`packageprice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `package_price` */

insert  into `package_price`(`packageprice_id`,`package_name`,`package_price`) values (1,'2 MB',850.00),(2,'4 MB',1500.00),(3,'6 MB',2000.00),(4,'8 MB',2500.00);

/*Table structure for table `payment` */

DROP TABLE IF EXISTS `payment`;

CREATE TABLE `payment` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `payment_amount` double(12,2) DEFAULT '0.00',
  `payment_date` date DEFAULT NULL,
  PRIMARY KEY (`payment_id`),
  KEY `FK_payment` (`employee_id`),
  CONSTRAINT `FK_payment` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

/*Data for the table `payment` */

/*Table structure for table `paymentgenerate` */

DROP TABLE IF EXISTS `paymentgenerate`;

CREATE TABLE `paymentgenerate` (
  `paymentgenerate_Id` int(11) NOT NULL AUTO_INCREMENT,
  `month` varchar(125) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`paymentgenerate_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

/*Data for the table `paymentgenerate` */

/*Table structure for table `role` */

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `role` */

insert  into `role`(`id`,`name`,`description`) values (1,'ROLE_USER',NULL),(2,'ADMIN',NULL),(3,'MASTER_ADMIN',NULL),(4,'DATA_ENTRY',NULL);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`id`,`username`,`password`,`age`) values (4,'mongadisha','$2a$11$8XrWnFZY/dQFHNT9fEBuPeazwg9VCbgu06QQnUwtDx5swsc0bDI1G',15),(5,'admin1234','$2a$11$9no.YBS8XTGEOrYNeWTKfeBeHvhIjVA9f1ZIcb2AFoJQh1nAoCCvC',NULL);

/*Table structure for table `user_auth` */

DROP TABLE IF EXISTS `user_auth`;

CREATE TABLE `user_auth` (
  `auth_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) NOT NULL,
  `email_id` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `isActive` int(2) NOT NULL DEFAULT '0',
  `user_type` varchar(40) NOT NULL DEFAULT 'Doner',
  `user_id` varchar(40) NOT NULL DEFAULT 'Doner',
  PRIMARY KEY (`auth_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `user_auth` */

insert  into `user_auth`(`auth_id`,`user_name`,`email_id`,`password`,`isActive`,`user_type`,`user_id`) values (11,'qadir','admin@wiztech.pk','$2a$11$nhRpS1LUmCu/xc1Jx6HlRO/ra7gnZhLAEgCF8kGgUd2bT8KXgVos2',0,'monga','3'),(12,'uzairalam','uzairalam94@dsadas.com','$2a$11$pByI/y3dkfI8E7VKi3BK8ehlpRAeOVpLBe2yBNC8INgrfItdk2l4q',0,'monga','3'),(13,'muhammadhashim001','muhammadhashim001@gmail.com','$2a$11$3REGqMdo2n5Qx/2W3AqevOisK1nhCAxMA3t0IUXhpnt1urkqR62nO',0,'monga','3'),(14,'muhammadhashim00','muhammadhashim001@gmail.com','$2a$11$Mw7oFUU6e5F3fzHyBwXVnOtJDDceJfmxMz0nPGCPv3eW1IbzaCqPu',0,'monga','3');

/*Table structure for table `user_role` */

DROP TABLE IF EXISTS `user_role`;

CREATE TABLE `user_role` (
  `auth_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`auth_id`,`role_id`),
  KEY `fk_user_role_roleid_idx` (`role_id`),
  CONSTRAINT `fk_user_role_roleid` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`auth_id`) REFERENCES `user_auth` (`auth_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user_role` */

insert  into `user_role`(`auth_id`,`role_id`) values (11,1),(12,1),(11,3);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
