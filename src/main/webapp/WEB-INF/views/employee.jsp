<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Home - Customer Manager</title>
    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">
    <link href="https://cdn.datatables.net/rowreorder/1.2.5/css/rowReorder.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->
    <style>
        .uploadImage {
            border: 1px solid #000;
            max-width: 300px;
            margin: 0 auto;
            height: 200px
        }

        .uploadImage .image {
            height: 135px;
            width: 100%;
            border-bottom: 1px solid #000;
            padding: 2px;
        }

        .uploadImage .image img {
            width: 100%;
            height: 100%
        }

        input[type="file"] {
            border-bottom: 0;
            margin-top: 5px
        }

        body {
            font-family: Arial, Helvetica, sans-serif;
        }

        .myImg {
            border-radius: 5px;
            cursor: pointer;
            transition: 0.3s;
        }

        .myImg:hover {
            opacity: 0.7;
        }

        /* The Modal (background) */
        .imgmodal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0, 0, 0); /* Fallback color */
            background-color: rgba(0, 0, 0, 0.9); /* Black w/ opacity */
        }

        /* Modal Content (image) */
        .modal-image-content {
            margin: auto;
            display: block;
            width: 80%;
            max-width: 700px;
        }

        /* Caption of Modal Image */
        #caption {
            margin: auto;
            display: block;
            width: 80%;
            max-width: 700px;
            text-align: center;
            color: #ccc;
            padding: 10px 0;
            height: 150px;
        }

        /* Add Animation */
        .modal-image-content, #caption {
            -webkit-animation-name: zoom;
            -webkit-animation-duration: 0.6s;
            animation-name: zoom;
            animation-duration: 0.6s;
        }

        @-webkit-keyframes zoom {
            from {
                -webkit-transform: scale(0)
            }
            to {
                -webkit-transform: scale(1)
            }
        }

        @keyframes zoom {
            from {
                transform: scale(0)
            }
            to {
                transform: scale(1)
            }
        }

        /* The Close Button */
        .close {
            position: absolute;
            top: 15px;
            right: 35px;
            color: #f1f1f1;
            font-size: 40px;
            font-weight: bold;
            transition: 0.3s;
        }

        .close:hover,
        .close:focus {
            color: #bbb;
            text-decoration: none;
            cursor: pointer;
        }

        /* 100% Image Width on Smaller Screens */
        @media only screen and (max-width: 700px) {
            .modal-image-content {
                width: 100%;
            }
        }

        .rotate90 {
            -webkit-transform: rotate(270deg);
            -moz-transform: rotate(270deg);
            -o-transform: rotate(270deg);
            -ms-transform: rotate(270deg);
            transform: rotate(270deg);
        }

    </style>
</head>

<body>

<%@ include file="navigation.jsp" %>

<section class="main">
    <div class="container">

        ${salaryGenerationSuccess}
        ${employeeRegisterSuccess}
        ${paymentAlreadyPaid}
        ${paymentNotSuccess}
        ${paymentGenerationSuccess}
        ${paymentGenerationNotSuccess}
        ${employeeSalaryDetailSuccess}
        ${employeeUpdateSuccess}
        ${duplicateSalaryDetails}
        ${salarySlipSuccess}
        ${salarySlipSuccess}
        ${packagePriceUpdated}
        ${paymentGenerateforMonth}
        ${paymentMessage}
        ${paymentSuccess}
        ${dbCreated}

        <h2 class="heading-main">Users <span class="addIcon" data-toggle="modal"
                                             data-target="#myModal2"><i><img
                src="${contextPath}/resources/images/add.png" alt=""></i>Add</span>
            <span class="addIcon" data-toggle="modal"
                  data-target="#myModal3"><i><img
                    src="${contextPath}/resources/images/view.png" alt=""></i>Balance</span></h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTable">
                        <thead>
                        <tr>
                            <th style="width:10%">Name</th>
                            <th style="width:10%">Image</th>
                            <th style="width:10%">Expiry Date</th>
                            <th style="width:10%">Active</th>
                            <th style="width:10%">Location</th>
                            <th style="width:10%">Package</th>
                            <th style="width:10%">Mobile</th>
                            <th style="width:10%">Balance</th>
                            <th style="width:10%">Payment</th>
                            <th style="width:10%">Action</th>


                        </tr>
                        </thead>
                        <tbody>


                        <c:forEach items="${employee}" var="employee" varStatus="status">

                            <tr>
                                <td>
                                    <a href="${contextPath}/Reports/paymentReport?employeeId=${employee.employeeId}&fromDate=${fromDate}&toDate=${currentDate}"
                                    ><h4>${employee.name}</h4></a></td>
                                <td>
                                    <c:choose>
                                        <c:when test="${employee.imageUrl == null || employee.imageUrl == ''}">
                                            No Image
                                        </c:when>
                                        <c:otherwise>
                                            <img src="${employee.imageUrl}" id="myImg${status.index}"
                                                 class="myImg rotate90" onclick="modalOpen(${status.index});"
                                                 alt="${employee.name}" height="20" width="20"/>
                                        </c:otherwise>
                                    </c:choose>


                                </td>
                                <td><fmt:formatDate pattern="dd/MMM/yyyy" value="${employee.joinDate}"/></td>
                                <c:choose>


                                    <c:when test="${employee.isActive == 1}">

                                        <td><img src="${contextPath}/resources/images/active.png" height="32"
                                                 width="32"/>
                                        </td>

                                    </c:when>
                                    <c:otherwise>
                                        <td><img src="${contextPath}/resources/images/inactive.png" height="32"
                                                 width="32"/>
                                        </td>

                                    </c:otherwise>
                                </c:choose>
                                <td>${employee.location.locationName}</td>
                                <td>${employee.status}</td>
                                <td>${employee.phone}</td>
                                <td>${employee.balance}</td>
                                <c:choose>
                                    <c:when test="${employee.paid == false}">
                                        <td><img src="${contextPath}/resources/images/thumbs-down.png"
                                                 height="32"
                                                 width="32"/></td>

                                    </c:when>
                                    <c:otherwise>
                                        <td><img src="${contextPath}/resources/images/thumbs-up.png" height="32"
                                                 width="32"/>
                                        </td>

                                    </c:otherwise>
                                </c:choose>
                                <td>
                                    <form method="post" id="uploadImage${status.index}"
                                          enctype="multipart/form-data"
                                          action="${contextPath}/uploadProfilePic">

                                        <input type="hidden" name="urlPath"
                                               value="${contextPath}/resources/images"/>
                                        <input type="hidden" name="employeeId" value="${employee.employeeId}"/>
                                        <input type="file" name="profileImage" id="originalFile${status.index}"
                                               onchange="uploadImage(${status.index});"/>
                                    </form>
                                    <form method="post" id="employeeRemove${status.index}"
                                          action="${contextPath}/removeEmployee">

                                        <input type="hidden" name="employeeId"
                                               value="${employee.employeeId}"/>
                                    </form>
                                    <ul class="list-inline">
                                        <li><a title="payment" href="#"
                                               onclick="editPaymentForm(${employee.employeeId})"><img
                                                src="${contextPath}/resources/images/bill.png" alt=""></a></li>
                                        <li><a title="edit" href="#" onclick="edit(${employee.employeeId})"
                                               class="editForm"><img
                                                src="${contextPath}/resources/images/edit.png" alt=""></a></li>
                                        <li><a title="delete" href="#" onclick="remove(${status.index})"><img
                                                src="${contextPath}/resources/images/remove.png" alt=""></a>
                                        </li>

                                    </ul>

                                </td>


                            </tr>

                        </c:forEach>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</section>
<!-- The Modal -->
<div id="imageModal" class="imgmodal">
    <span data-dismiss="modal" aria-label="Close" aria-hidden="true" class="closeFixed">&times;</span>
    <span class="close">&times;</span>
    <img class="modal-image-content rotate90" id="img01">
    <div id="caption"></div>
</div>
<!-- Modal -->
<div class="modal fade modal-small" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: #1f2d5c" style=" color: white">
            <div class="modal-header">
                <span data-dismiss="modal" aria-label="Close" aria-hidden="true" class="closeFixed">&times;</span>
                <h2 class="modal-title" id="myModalLabel">Add User</h2>
            </div>

            <form:form method="POST" modelAttribute="detailForm" enctype="multipart/form-data" action="./userdetails">
            <div class="modal-body">
                <span id="messageDiv"></span>
                <spring:bind path="name">
                    <form:label path="name">Name</form:label>
                    <form:input type="text" path="name" required="true" class="form-control" placeholder="Name"
                                autofocus="true"></form:input>
                    <form:errors path="name"></form:errors>
                </spring:bind>


                <spring:bind path="cnic">
                    <form:label path="cnic">CNIC</form:label>
                    <form:input type="text" path="cnic" required="true" class="form-control"
                                placeholder="CNIC number"></form:input>
                    <form:errors path="cnic"></form:errors>
                </spring:bind>
                <spring:bind path="macAddress">
                    <div class="form-group">
                        <form:label path="macAddress">User MAC Address</form:label>
                        <form:textarea type="text" path="macAddress" class="form-control"
                                       placeholder="MAC Address"></form:textarea>
                        <form:errors path="macAddress"></form:errors>
                    </div>
                </spring:bind>

                <spring:bind path="phone">
                    <form:label path="phone">Mobile</form:label>
                    <form:input type="text" path="phone" class="form-control" placeholder="Mobile"></form:input>
                    <form:errors path="phone"></form:errors>
                </spring:bind>
                <spring:bind path="balance">
                    <form:label path="balance">Balance Amount</form:label>
                    <form:input type="number" path="balance" class="form-control" value="0"></form:input>
                    <form:errors path="balance"></form:errors>
                </spring:bind>

                <spring:bind path="monthlyAmount">
                    <form:label path="monthlyAmount">Monthly Amount</form:label>
                    <form:input type="number" path="monthlyAmount" class="form-control" value="0"></form:input>
                    <form:errors path="monthlyAmount"></form:errors>
                </spring:bind>
                <spring:bind path="joinDate">
                    <form:label path="joinDate">Date of Joining</form:label>
                    <form:input type="Date" path="joinDate" class="form-control" value="${currentDate}"
                                placeholder="Joining Date"/>
                    <form:errors path="joinDate"/>
                </spring:bind>
                <spring:bind path="expiryDate">
                    <form:label path="expiryDate">Expiry Date</form:label>
                    <form:input type="Date" path="expiryDate" class="form-control" value="${currentDate}"
                                placeholder="Expiry Date"/>
                    <form:errors path="expiryDate"/>
                </spring:bind>


                <spring:bind path="status">
                    <div class="form-group">
                        <form:label path="status">Package</form:label>
                        <form:select path="status" class="form-control">

                            <form:option value="2 MB">2 MB</form:option>
                            <form:option value="4 MB">4 MB</form:option>
                            <form:option value="6 MB">6 MB</form:option>
                            <form:option value="8 MB">8 MB</form:option>
                            <form:option value="10 MB">10 MB</form:option>
                        </form:select>

                    </div>
                </spring:bind>
                <spring:bind path="locationId">
                    <div class="form-group">
                        <form:label path="locationId">Location</form:label>
                        <form:select path="locationId" class="form-control">
                            <c:forEach items="${locationList}" var="location">
                                <form:option value="${location.locationId}">${location.locationName}</form:option>
                            </c:forEach>
                        </form:select>

                    </div>
                </spring:bind>
                <spring:bind path="isActive">
                    <div class="form-group">
                        <form:label path="isActive">Active Status</form:label>
                        <form:select path="isActive" class="form-control">
                            <form:option value="1">Active</form:option>
                            <form:option value="0">Not Active</form:option>

                        </form:select>

                    </div>
                </spring:bind>


                <spring:bind path="address">
                    <form:label path="address">User Address</form:label>
                    <form:textarea type="text" path="address" class="form-control"
                                   placeholder="Address"></form:textarea>
                    <form:errors path="address"></form:errors>
                </spring:bind>
                <spring:bind path="cnicFront">
                    <form:label path="cnicFront">Select File to upload Front CNIC</form:label>

                    <div class="uploadImage">
                        <div class="image">
                            <img id="frontCnic" src="${updateDetailForm.cnicFrontUrl}"/>
                        </div>
                        <form:input path="cnicFront" type="file" value=""
                                    onchange="readURL(this,'frontCnic');"></form:input>
                    </div>
                </spring:bind>
                <spring:bind path="cnicBack">
                    <form:label path="cnicBack">Select File to upload Back CNIC</form:label>

                    <div class="uploadImage">
                        <div class="image">
                            <img id="backCnic" src="${updateDetailForm.cnicBackUrl}"/>
                        </div>
                        <form:input path="cnicBack" type="file" value=""
                                    onchange="readURL(this,'backCnic');"></form:input>
                    </div>
                </spring:bind>
                <form:input type="hidden" path="cincPath" value="${contextPath}/resources/images"></form:input>
                <form:input type="hidden" path="employeeId" value="0"></form:input>
                <form:input type="hidden" path="imageUrl" value=""></form:input>
                <form:input type="hidden" path="paid" value="0"></form:input>
                <div class="modal-footer" style="background-color: #1f2d5c">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="submit">Submit</button>
                        </div>
                        <div class="col-sm-6 text-center">
                            <button type="button" data-dismiss="modal">Exit</button>
                        </div>
                    </div>
                </div>
                </form:form>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade modal-small" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: #1f2d5c" style=" color: white">
            <div class="modal-header">
                <span data-dismiss="modal" aria-label="Close" aria-hidden="true" class="closeFixed">&times;</span>
                <h2 class="modal-title" id="myModalLabel1">Add Payment</h2>
            </div>

            <form:form method="POST" modelAttribute="paymentForm" action="${contextPath}/paymentdetails/addPayment">
                <div class="modal-body">
                    <spring:bind path="employeeId">
                        <form:label path="employeeId">User</form:label>
                        <form:select path="employeeId">
                            <c:forEach items="${employee}" var="employee">
                                <form:option value="${employee.employeeId}">${employee.name}</form:option>
                            </c:forEach>
                        </form:select>
                    </spring:bind>
                    <spring:bind path="month">

                        <form:label path="month">Month</form:label>
                        <form:select path="month">
                            <c:forEach items="${monthMap}" var="month">
                                <c:choose>
                                    <c:when test="${currentMonth == month.key}">
                                        <form:option value="${month.key}"
                                                     selected="true">${month.value}</form:option>
                                    </c:when>
                                    <c:otherwise>
                                        <form:option value="${month.key}">${month.value}</form:option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </form:select>
                    </spring:bind>
                    <spring:bind path="year">

                        <form:label path="year">Year</form:label>
                        <form:select path="year">
                            <c:forEach items="${yearList}" var="year">
                                <c:choose>
                                    <c:when test="${currentYear == year}">
                                        <form:option value="${year}" selected="true">${year}</form:option>
                                    </c:when>
                                    <c:otherwise>
                                        <form:option value="${year}">${year}</form:option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </form:select>
                    </spring:bind>
                    <spring:bind path="paymentAmount">
                        <form:label path="paymentAmount">Payment Amount</form:label>
                        <form:input type="number" path="paymentAmount" placeholder="Amount" value="0"
                                    required="true"></form:input>
                        <form:errors path="paymentAmount"></form:errors>
                    </spring:bind>
                    <spring:bind path="paymentDate">
                        <form:label path="paymentDate">Payment Date</form:label>
                        <form:input type="date" path="paymentDate" placeholder="Amount" value="${currentDate}"
                                    required="true"></form:input>
                        <form:errors path="paymentDate"></form:errors>
                    </spring:bind>
                </div>
                <spring:bind path="paymentId">
                    <form:input type="hidden" path="paymentId"
                                value="0"></form:input>
                </spring:bind>
                <spring:bind path="previousAmount">
                    <form:input type="hidden" path="previousAmount"
                                value="0"></form:input>
                </spring:bind>
                <spring:bind path="previousEmployee">
                    <form:input type="hidden" path="previousEmployee"
                                value="0"></form:input>
                </spring:bind>
                <div class="modal-footer" style="background-color: #1f2d5c">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="submit">Submit</button>
                        </div>
                        <div class="col-sm-6 text-center">
                            <button type="button" data-dismiss="modal">Exit</button>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>
<div class="modal fade modal-small" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: #1f2d5c" style=" color: white">
            <div class="modal-header">
                <span data-dismiss="modal" aria-label="Close" aria-hidden="true" class="closeFixed">&times;</span>
                <h2 class="modal-title" id="myModalLabel3">Balance Amount</h2>
            </div>

            <div class="modal-body">
                <span><b>Total Amount: </b> ${balanceAmount+monthlyAmount}</span><br/>
                <span><b>Total Payment: </b> ${monthlyAmount}</span><br/>
                <span><b>Balance Amount: </b> ${balanceAmount}</span>
                <div class="modal-footer" style="background-color: #1f2d5c">
                    <div class="row">
                        <div class="text-center">
                            <button type="button" data-dismiss="modal">Exit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/dataTables/datatable.js"></script>
<script src="https://cdn.datatables.net/rowreorder/1.2.5/js/dataTables.rowReorder.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script>
    var form_action = "add";

    function editPaymentForm(response) {
        console.log(response);
        $('#myModal').modal();
        $('#myModal').find('form').trigger('reset');
        $('#myModal').find('form').find('#employeeId').val(response);

    }

    function remove(id) {
        if (confirm("Are you sure, you want to delete?")) {
            $('#employeeRemove' + id).submit();
        }
    }

    $(document).ready(function () {
        $('#dataTable').DataTable({
            "order": [[0, "asc"]],
            "pageLength": 100
        });
    });
    $(".editForm").click(function () {
        form_action = 'edit';
    });

    function readURL(input, id) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                if (id == 'frontCnic') {
                    $('#frontCnic')
                        .attr('src', e.target.result);
                } else if (id == 'backCnic') {
                    $('#backCnic')
                        .attr('src', e.target.result);
                } else {
                    $('#profilePic')
                        .attr('src', e.target.result);
                }
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function edit(employeeId) {

        $.ajax({
            url: '${contextPath}/editEmployee',
            contentType: 'application/json',

            data: {
                employeeId: employeeId

            },
            success: function (response) {

                editForm(response);
            },

            error: function (e) {

                console.log("ERROR : ", e);

            }
        });
    }

    function editForm(response) {
        $('#myModal2').modal();
        $('#myModal2').find('form').trigger('reset');
        $('#myModal2').find('form').find('#employeeId').val(response.employeeId);
        $('#myModal2').find('form').find('#imageUrl').val(response.imageUrl);
        $('#myModal2').find('form').find('#paid').val(response.paid);
        $('#myModal2').find('form').find('#name').val(response.name);
        $('#myModal2').find('form').find('#cnic').val(response.cnic);
        $('#myModal2').find('form').find('#macAddress').val(response.macAddress);
        $('#myModal2').find('form').find('#balance').val(response.balance);
        $('#myModal2').find('form').find('#joinDate').val(response.joinDate);
        $('#myModal2').find('form').find('#expiryDate').val(response.expiryDate);
        $('#myModal2').find('form').find('#status').val(response.status);
        $('#myModal2').find('form').find('#isActive').val(response.isActive);
        $('#myModal2').find('form').find('#address').val(response.address);
        $('#myModal2').find('form').find('#cnicFront').val(response.cnicFront);
        $('#myModal2').find('form').find('#cnicBack').val(response.cnicBack);
        $('#myModal2').find('form').find('#monthlyAmount').val(response.monthlyAmount);
        $('#myModal2').find('form').find('#locationId').val(response.locationId);

        $('#frontCnic').attr('src', response.cnicFrontUrl);
        $('#backCnic').attr('src', response.cnicBackUrl);

    }

    function addVoucherForm() {
        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");

        $(document).ajaxSend(function (e, xhr, options) {
            xhr.setRequestHeader(header, token);
        });
        var fd = $("#detailForm").serialize();
        $.ajax({
            type: 'POST',
            async: false,
            url: '${contextPath}/Account/userDetails',
            data: fd,
            timeout: 600000,
            success: function (data) {
                if (form_action == "edit") {
                    refreshPage();
                } else {
                    updateForm();
                }
            },
            error: function (e) {

                console.log("ERROR : ", e);

            }
        });
    }

    function updateForm() {
        $('#myModal').find('form').trigger('reset');
        $('#myModal').find('form').find('messageDiv').html('<div style=\\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\\">User has been registered successfully.</div>');

    }

    function refreshPage() {
        window.location.reload();
    }

    function uploadImage(id) {
        $('#uploadImage' + id).submit();
    }

    // Get the modal
    var modal = document.getElementById('imageModal');
    var modalImg = document.getElementById("img01");

    function modalOpen(id) {

        // Get the image and insert it inside the modal - use its "alt" text as a caption
        var img = document.getElementById('myImg' + id);
        var captionText = document.getElementById("caption");
        modal.style.display = "block";
        modalImg.src = img.src;
        captionText.innerHTML = img.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }

</script>

</body>
</html>
