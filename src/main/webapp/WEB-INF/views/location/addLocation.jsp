<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Locations</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../navigation.jsp" %>


<section class="main">
    <div class="container">
        ${locationAddSuccess}
        ${locationRemoveSuccess}
        <h2 class="heading-main">Locations <span class="addIcon" data-toggle="modal"
                                                 data-target="#myModal"><i><img
                src="${contextPath}/resources/images/add.png" alt=""></i>Add</span></h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTable">
                        <thead>
                        <tr>
                            <th>Location ID</th>
                            <th>Location Name</th>
                            <th>Total Users</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${locations}" var="location" varStatus="status">

                            <tr>
                                <td>${location.locationId}</td>
                                <td><a href="${contextPath}/Location/users?locationId=${location.locationId}"><h4>${location.locationName}</h4></a></td>
                                <td>${location.employees.size()}</td>
                                <td>
                                    <form method="post" id="locationRemove${status.index}"
                                          action="${contextPath}/Location/removeLocation">
                                        <input type="hidden" name="locationId"
                                               value="${location.locationId}"/>
                                    </form>
                                    <ul class="list-inline">
                                        <li><a href="#" onclick="edit(${location.locationId});"><img
                                                src="${contextPath}/resources/images/edit.png" alt=""></a></li>
                                     <%--   <li><a href="#" onclick="remove(${status.index});"><img
                                                src="${contextPath}/resources/images/remove.png" alt=""></a></li>--%>
                                     </ul>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade modal-small" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: #1f2d5c" style=" color: white">
            <div class="modal-header">
                <span data-dismiss="modal" aria-label="Close" aria-hidden="true" class="closeFixed">&times;</span>
                <h3 class="modal-title" id="myModalLabel">Add Location</h3>
            </div>

            <form:form method="POST" modelAttribute="locationForm">
                <div class="modal-body">
                    <spring:bind path="locationName">

                        <form:label path="locationName">Location</form:label>
                        <form:input type="text" path="locationName" placeholder="Location" id="locationName"
                                    autofocus="true" required="true" ></form:input>
                        <form:errors path="locationName"></form:errors>

                    </spring:bind>

                    <spring:bind path="locationId">
                        <form:input type="hidden" path="locationId" id="locationId"
                                    autofocus="true"></form:input>

                    </spring:bind>
                </div>

                <div class="modal-footer" style=" background-color: #1f2d5c">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="submit">Submit</button>
                        </div>
                        <div class="col-sm-6 text-center">
                            <button type="button" data-dismiss="modal">Exit</button>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script>

    $(document).ready(function () {
        $('#dataTable').DataTable({
            "order": [[0, "desc"]],
            "pageLength": 7
        });
    });
    function goBack(){
        window.location = "${contextPath}/home#masterFile";
    }

    function remove(id){
        if(confirm("Are you sure you want to remove the Location")){
            $('#locationRemove'+id).submit();
        }
    }

    function edit(locationId) {
        $.ajax({
            url: '${contextPath}/Location/editLocation',
            contentType: 'application/json',
            data: {
                locationId: locationId

            },
            success: function (response) {
                editForm(response);
            },
            error: function (e) {

                console.log("ERROR : ", e);

            }
        });
    }
    function editForm(response) {
        $("#myModal").modal();
        $('#myModal').find('form').trigger('reset');
        $('#myModal').find('form').find('#locationName').val(response.locationName);
        $('#myModal').find('form').find('#locationId').val(response.locationId);
    }
</script>
</body>
</html>
