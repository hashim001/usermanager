<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Locations</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../navigation.jsp" %>


<section class="main">
    <div class="container">


        <div align="right">
            <div class="pdfLinkSec">
                <button class="btn btn-success" style="background-color: #292e5a" id="printBtn">Print
                </button>
            </div>

        </div>
        <div class="row"  id="printDiv">

            <h2 class="heading-main">Location Name : ${location.locationName} </h2>

            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Sno</th>
                            <th>User Name</th>
                            <th>Status</th>
                            <th>Phone#</th>
                            <th>Address</th>
                            <th>Balance</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${location.employees}" var="employee" varStatus="status">

                            <tr>
                                <td>${status.index+1}</td>
                                <td>${employee.name}</td>
                                <c:choose>
                                    <c:when test="${employee.isActive == 1}">
                                        <td>Active</td>
                                    </c:when>
                                    <c:otherwise>
                                        <td>InActive</td>
                                    </c:otherwise>
                                </c:choose>
                                <td>${employee.phone}</td>
                                <td>${employee.address}</td>
                                <td>${employee.balance}</td>

                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script>

    $('#printBtn').click(function () {
        printDiv();
    });

    function printDiv() {

        var divToPrint = document.getElementById('printDiv');
        var ledgerReportDataInfo = document.getElementById('ledgerReportDataInfo');
        var newWin = window.open('', 'Print-Window');

        newWin.document.open();

        newWin.document.write('<html><head>\n' +
            '\t\n' +
            '\n' +
            '\t\n' +
            '\t<style type="text/css">\n' +
            '\t\tbody,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Calibri"; font-size:small }\n' +
            '\t\ta.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  } \n' +
            '\t\ta.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  } \n' +
            '\t\tcomment { display:none;  } \n' +
            '\t</style>\n' +
            '<link href="${contextPath}/resources/css/ledgerreport-style.css" rel="stylesheet">' +
            '<link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">' +
            ' <link href="${contextPath}/resources/css/style.css" rel="stylesheet">' +
            '</head>\n<body onload="window.print()">' +
            divToPrint.innerHTML +
            '</body></html>');

        newWin.document.close();
        setTimeout(function () {
            newWin.close();
        }, 50000);

    }

</script>
</body>
</html>
