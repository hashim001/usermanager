<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Monthly Payment Generation</title>
        <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
        <link href="${contextPath}/resources/css/common.css" rel="stylesheet">

    </head>
    <body>
        <%@ include file = "navigation.jsp" %>
        <div class="container">
            <h2>Payment for month of ${current_month}</h2>
            <form:form method="POST" modelAttribute="employeesList" class="form-signin">

                <h2 class="form-signin-heading">Monthly Payment Generation</h2>
                <table class="table table-condensed">
        
                    <td>           <select name="month" class="form-control">
                            <c:forEach items="${monthList}" var="month">
                                <option value="${month}">${month}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <td>            <select name="year"   class="form-control">
                            <c:forEach items="${yearList}" var="year">
                                <option value="${year}">${year}</option>
                            </c:forEach>

                        </select>
                    </td>
                </table>

                <input class="btn btn-success pull-right" type="submit" value="Submit Payment"/>


            </form:form>
        </div>

        <script src="${contextPath}/resources/js/jquery.min.js"></script>
        <script src="${contextPath}/resources/js/bootstrap.min.js"></script>
    </body>
</html>
