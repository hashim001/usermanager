<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>User Details</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/style.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<style>
img {
          width:100%;
      }
      </style>
<body>
<%@ include file="navigation.jsp" %>

<div class="container">
${employeeUpdateSuccess}

    <h2>User Details</h2>
    <h3 style="text-transform: capitalize">Name: ${employee_details.name} </h3>

    <div class="row">

        <div class="col-md-4">

            <h4>CNIC: ${employee_details.cnic}</h4>
            <h4>Package: ${employee_details.status}</h4>
            <h4>Mobile: ${employee_details.phone}</h4>
            <h4>Joining Date: ${employee_details.joinDate}</h4>
            <h4>MAC Address: ${employee_details.macAddress}</h4>
            <h4>Address: ${employee_details.address}</h4>

        </div>
          <div class="col-md-4">
            <data><h3>Balance: ${employee_details.balance}</h3></data>
            <a href="${contextPath}/updateEmployee?employeeid=${employee_details.employeeId}"
               class="btn btn-lg btn-primary">Update User</a>
            <a href="${contextPath}/paymentdetails?employeeid=${employee_details.employeeId}"
               class="btn btn-lg btn-primary">User Payment</a>

        </div>
    </div>


    <div class="table-condensed">

        <h3>Payment Details</h3>

        <table class="table table-condensed">
            <thead>
            <tr>
                <th>Month</th>
                <th>Year</th>
                <th>Date of Entry</th>


            </tr>
            </thead>
            <tbody>
            <c:forEach items="${salary_script}" var="salary_script" varStatus="status">

                <tr>


                    <td>${salary_script.month}</td>
                    <td>${salary_script.year}</td>
                    <td>${salary_script.date}</td>


                </tr>
            </c:forEach>
            </tbody>
        </table>

    </div>
</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>
