<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Monthly Payments</title>
    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->
</head>

<body>

<%@ include file="navigation.jsp" %>

<section class="main">
    <div class="container">
    ${PaymentRemoveSuccess}
    ${paymentSuccess}
        <h2 class="heading-main">Monthly Payments</h2>
           <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTable">
                        <thead>
                        <tr>
                            <th>User</th>
                            <th>Date</th>
                            <th>Month</th>
                            <th>Year</th>
                            <th>Amount</th>
                            <th>Action</th>


                        </tr>
                        </thead>
                        <tbody>

                        <c:forEach items="${paymentList}" var="payment" varStatus="status">
                            <tr>
                              <td>${payment.employees.name}</td>
                              <td><fmt:formatDate type = "date" value = "${payment.paymentDate}" pattern="dd/MM/yyyy"/></td>
                              <td>${monthMap.get(payment.month)}</td>
                              <td>${payment.year}</td>
                              <td>${payment.paymentAmount}</td>
                               <td>
                                    <form method="post" id="paymentRemove${status.index}"
                                          action="${contextPath}/paymentdetails/removePayment?${_csrf.parameterName}=${_csrf.token}">
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                        <input type="hidden" name="paymentId"
                                               value="${payment.paymentId}"/>
                                    </form>
                                    <ul class="list-inline">
                                        <li><a title="edit" href="#" onclick="edit(${payment.paymentId})" ><img src="${contextPath}/resources/images/edit.png" alt=""></a></li>
                                        <li><a title="delete" href="#" style="" onclick="remove(${status.index})"><img src="${contextPath}/resources/images/remove.png" alt=""></a></li>

                                    </ul>

                               </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</section>

<!-- Modal -->
<div class="modal fade modal-small" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: #1f2d5c" style=" color: white">
            <div class="modal-header">
                <span data-dismiss="modal" aria-label="Close" aria-hidden="true" class="closeFixed">&times;</span>
                <h2 class="modal-title" id="myModalLabel">Add Payment</h2>
            </div>

            <form:form method="POST" modelAttribute="paymentForm">
                <div class="modal-body">
                    <spring:bind path="employeeId">
                        <form:label path="employeeId">User</form:label>
                        <form:select path="employeeId" >
                            <c:forEach items="${employeeList}" var="employee">
                                <form:option value="${employee.employeeId}">${employee.name}</form:option>
                            </c:forEach>
                        </form:select>
                    </spring:bind>
                    <spring:bind path="month">

                        <form:label path="month">Month</form:label>
                        <form:select path="month" >
                            <c:forEach items="${monthMap}" var="month">
                            <c:choose>
                            <c:when test="${currentMonth == month.key}">
                                <form:option value="${month.key}" selected="true">${month.value}</form:option>
                            </c:when>
                            <c:otherwise>
                                <form:option value="${month.key}">${month.value}</form:option>
                            </c:otherwise>
                            </c:choose>
                            </c:forEach>
                        </form:select>
                    </spring:bind>
                    <spring:bind path="year">

                        <form:label path="year">Year</form:label>
                        <form:select path="year" >
                            <c:forEach items="${yearList}" var="year">
                            <c:choose>
                            <c:when test="${currentYear == year}">
                                <form:option value="${year}" selected="true">${year}</form:option>
                            </c:when>
                            <c:otherwise>
                                <form:option value="${year}">${year}</form:option>
                            </c:otherwise>
                            </c:choose>
                            </c:forEach>
                        </form:select>
                    </spring:bind>
                    <spring:bind path="paymentAmount">
                        <form:label path="paymentAmount">Payment Amount</form:label>
                        <form:input type="number" path="paymentAmount" placeholder="Amount" value="0" required ="true" ></form:input>
                        <form:errors path="paymentAmount"></form:errors>
                    </spring:bind>
                    <spring:bind path="paymentDate">
                        <form:label path="paymentDate">Payment Date</form:label>
                        <form:input type="date" path="paymentDate" placeholder="Amount" value="${currentDate}" required ="true" ></form:input>
                        <form:errors path="paymentDate"></form:errors>
                    </spring:bind>
                </div>
                          <spring:bind path="paymentId">
                                <form:input type="hidden" path="paymentId"
                                            value="0"></form:input>
                            </spring:bind>
                          <spring:bind path="previousAmount">
                                <form:input type="hidden" path="previousAmount"
                                            value="0"></form:input>
                            </spring:bind>
                          <spring:bind path="previousEmployee">
                                <form:input type="hidden" path="previousEmployee"
                                            value="0"></form:input>
                            </spring:bind>
                <div class="modal-footer" style="background-color: #1f2d5c" >
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="submit">Submit</button>
                        </div>
                        <div class="col-sm-6 text-center">
                            <button type="button" data-dismiss="modal">Exit</button>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/dataTables/datatable.js"></script>
<script>

    function remove(id) {
        if (confirm("Are you sure, you want to delete?")) {
            $('#paymentRemove' + id).submit();
        }
    }
    $(document).ready(function () {
        $('#dataTable').DataTable({
            "order": [[1, "desc"]],
            "pageLength": 7
        });
    });
            function edit(paymentId) {

                $.ajax({
                    url: '${contextPath}/paymentdetails/ajax/editPayment',
                    contentType: 'application/json',

                    data: {
                        paymentId: paymentId

                    },
                    success: function (response) {

                        editForm(response);
                    },

                    error: function (e) {

                        console.log("ERROR : ", e);

                    }
                });
            }
    		    function editForm(response) {
                 $('#myModal').modal();
                 $('#myModal').find('form').trigger('reset');
                 $('#myModal').find('form').find('#employeeId').val(response.employeeId);
                 $('#myModal').find('form').find('#month').val(response.month);
                 $('#myModal').find('form').find('#year').val(response.year);
                 $('#myModal').find('form').find('#paymentAmount').val(response.paymentAmount);
                 $('#myModal').find('form').find('#paymentDate').val(response.paymentDate);
                 $('#myModal').find('form').find('#paymentId').val(response.paymentId);
                 $('#myModal').find('form').find('#previousAmount').val(response.paymentAmount);
                 $('#myModal').find('form').find('#previousEmployee').val(response.employeeId);

            }

</script>

</body>
</html>
