<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Add User</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
        .uploadImage{
            border: 1px solid #000;
            max-width: 300px;
            margin: 0 auto;
            height: 200px
        }
        .uploadImage .image{height: 135px;width: 100%;border-bottom: 1px solid #000;padding: 2px;}
        .uploadImage .image img{width: 100%;height: 100%}
        input[type="file"]{border-bottom: 0;margin-top: 5px}
</style>
</head>

<body>

<%@ include file="navigation.jsp" %>

<div class="container">

    <form:form method="POST"  action="./userdetails?${_csrf.parameterName}=${_csrf.token}"
               modelAttribute="detailForm"   enctype="multipart/form-data"
               cssStyle="padding-left: 250px;padding-right: 360px;">
        <h2 class="form-signin-heading">User Registration</h2>


        <spring:bind path="name">
                <form:label path="name">Name</form:label>
                <form:input type="text" path="name" required="true" class="form-control" placeholder="Name"
                            autofocus="true"></form:input>
                <form:errors path="name"></form:errors>
        </spring:bind>


        <spring:bind path="cnic">
                <form:label path="cnic">CNIC</form:label>
                <form:input type="text" path="cnic" required="true" class="form-control" placeholder="CNIC number"></form:input>
                <form:errors path="cnic"></form:errors>
        </spring:bind>
        <spring:bind path="macAddress">
            <div class="form-group">
                <form:label path="macAddress">User MAC Address</form:label>
                <form:textarea type="text" path="macAddress" class="form-control"
                               placeholder="MAC Address"></form:textarea>
                <form:errors path="macAddress"></form:errors>
            </div>
        </spring:bind>

        <spring:bind path="phone">
                <form:label path="phone">Mobile</form:label>
                <form:input type="text" path="phone"  class="form-control" placeholder="Mobile"></form:input>
                <form:errors path="phone"></form:errors>
        </spring:bind>
        <spring:bind path="balance">
                <form:label path="balance">Balance Amount</form:label>
                <form:input type="number" path="balance" class="form-control" value="0"></form:input>
                <form:errors path="balance"></form:errors>
        </spring:bind>
        <spring:bind path="joinDate">
            <form:label path="joinDate">Date of Joining</form:label>
            <form:input type="Date" path="joinDate" class="form-control" value="${currentDate}"
                        placeholder="Joining Date"></form:input>
            <form:errors path="joinDate"></form:errors>
        </spring:bind>


        <spring:bind path="status">
            <div class="form-group">
                <form:label path="status">Package</form:label>
                <form:select path="status" class="form-control">

                    <form:option value="2 MB">2 MB</form:option>
                    <form:option value="4 MB">4 MB</form:option>
                    <form:option value="6 MB">6 MB</form:option>
                    <form:option value="8 MB">8 MB</form:option>

                </form:select>

            </div>
        </spring:bind>

        <spring:bind path="isActive">
            <div class="form-group">
                <form:label path="isActive">Active Status</form:label>
                <form:select path="isActive" class="form-control">
                    <form:option value="1">Active</form:option>
                    <form:option value="0">Not Active</form:option>

                </form:select>

            </div>
        </spring:bind>


        <spring:bind path="address">
                <form:label path="address">User Address</form:label>
                <form:textarea type="text" path="address" class="form-control"
                               placeholder="Address"></form:textarea>
                <form:errors path="address"></form:errors>
        </spring:bind>
                <spring:bind path="cnicFront">
               <form:label path="cnicFront">Select File to upload Front CNIC</form:label>

                        <div class="uploadImage">
                            <div class="image">
                                <img id="frontCnic" src="${detailForm.cnicFrontUrl}"/>
                            </div>
                            <form:input path="cnicFront" type="file" value="" onchange="readURL(this,'frontCnic');"></form:input>
                        </div>
               </spring:bind>
              <spring:bind path="cnicBack">
              <form:label path="cnicBack">Select File to upload Back CNIC</form:label>

                        <div class="uploadImage">
                            <div class="image">
                                <img id="backCnic" src="${detailForm.cnicBackUrl}"/>
                            </div>
                            <form:input path="cnicBack" type="file" value="" onchange="readURL(this,'backCnic');"></form:input>
                        </div>
              </spring:bind>
            <form:input type="hidden" path="cincPath"  value="${contextPath}/resources/images" ></form:input>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
    </form:form>

</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script>
    function readURL(input,id) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
            if(id == 'frontCnic')
            {
                $('#frontCnic')
                    .attr('src', e.target.result);
            }
            else
            {
                $('#backCnic')
                    .attr('src', e.target.result);
            }
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

</script>
</body>
</html>
