<jsp:useBean id="now" class="java.util.Date" />
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:formatDate var="currentDate" value="${now}" pattern="yyyy-MM-dd" />
<div class="container">

    <nav class="navbar navbar-inverse bg-inverse ">

        <a class="navbar-brand" href="${contextPath}/home">Home</a>
        <a class="navbar-brand" href="${contextPath}/paymentdetails/addPayment">Payments</a>
		<a class="navbar-brand" href="${contextPath}/paymentgenerate">Generate Monthly Payments</a>
        <a class="navbar-brand" href="${contextPath}/Reports/paymentReport?employeeId=0&fromDate=${currentDate}&toDate=${currentDate}">Payment Report</a>
        <a class="navbar-brand" href="${contextPath}/Location/addLocation">Add Location</a>
        <a class="navbar-brand" href="${contextPath}/dbBackUp">DB Backup</a>

        <%--
        <a class="navbar-brand" href="${contextPath}/packagerates">Update Package Rates</a>
--%>
        <form action="${contextPath}/logout" method="post">
            <input  class="navbar-brand pull-right" type="submit" value="Logout"/>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>


    </nav>

</div>