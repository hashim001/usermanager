<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Package Rates</title>
    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">

</head>
<body>

${pagmeMessage}
<%@ include file="navigation.jsp" %>

<div class="container">


    <form:form method="POST" modelAttribute="packagePriceList" class="form-signin">
        <h2 class="form-signin-heading">Package Rates</h2>
        <table class="table table-condensed">
            <thead>
            <tr>
                <th>Package Name</th>
                <th>Package Price</th>
            </tr>
            </thead>
            <tbody>


            <c:forEach items="${packagePriceList.packagePrices}" var="packageRate" varStatus="status">
                <tr>

                    <td>${packageRate.packageName}</td>
                    <td>

                        <form:select path="packagePrices[${status.index}].packagePrice" style="width: max-content"
                                     class="form-control">
                            <c:forEach items="${prices}" var="price">
                            <c:choose>
                            <c:when test="${packageRate.packagePrice == price}">
                                <option value="${price}" selected>${price}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${price}">${price}</option>
                            </c:otherwise>
                            </c:choose>
                            </c:forEach>
                        </form:select>

                    </td>

                </tr>

            </c:forEach>
            </tbody>
        </table>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>

    </form:form>
</div>
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>
