package com.hellokoding.account.constants;

public interface Constants {

    static int EOBI_DEDUCTION_VALUE =130;
    static int ALLOWED_LEAVES_PER_ANNUM = 18;
    static int MONTHLY_LEAVES_BOUND = 4;
    static Double LEAVES_PER_MONTH = 1.5;
    static String PAYMENT_MESSAGE = "<div style=\"background-color:red;height: 40px;text-align: center;padding-top: 5px;border: 2px solid red; color:white;border-radius: 5px;font-size: initial;\">Please generate payment for this Month.</div>";

}
