/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hellokoding.account.repository;

import com.hellokoding.account.model.Employee;
import com.hellokoding.account.model.Payment;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author muhammad
 */
public interface PaymentRepository extends JpaRepository<Payment, Long> {
    List<Payment> findByEmployees(Employee employee);

    @Query("Select e from Payment e  where e.month = :Month and e.year = :Year and e.employees = :EmployeeId")
    List<Payment> findByPaymentMonth(@Param("Month") String Month, @Param("Year") int Year, @Param("EmployeeId") Employee EmployeeId);


    @Query("Select e.month from Payment e where e.employees = :EmployeeId")
    List<String> findByEmployee(@Param("EmployeeId") Employee EmployeeId);
    
    

/*
    @Modifying(clearAutomatically = true)
    @Query("update Payment e set e.month = :Month , e.year= :Year , e.date = :Date WHERE e.paymentId =:PaymentId")
    void updatePayment(@Param("Month") String Month, @Param("Year") int Year, @Param("PaymentId") int PaymentId, @Param("Date") String Date);
*/

    Payment findByPaymentId(int paymentId);

    @Query(value = "SELECT * FROM payment WHERE MONTH >= ?1 AND MONTH <= ?2 AND YEAR = ?3  AND employee_id = ?4", nativeQuery = true)
    List<Payment> findPaymentByMonthAndYearAndEmployee(int fromMonth, int toMonth, int year, int employeeId);

    @Query(value = "SELECT * FROM payment WHERE MONTH >= ?1 AND MONTH <= ?2 AND YEAR = ?3", nativeQuery = true)
    List<Payment> findPaymentByMonthAndYear(int fromMonth, int toMonth, int year);

    @Query(value = "SELECT * FROM payment WHERE payment_date >= ?1 AND payment_date <= ?2  AND employee_id = ?3 order by payment_date asc", nativeQuery = true)
    List<Payment> findPaymentByDatesAndEmployee(Date fromDate, Date toDate, int employeeId);

    @Query(value = "SELECT * FROM payment WHERE payment_date >= ?1 AND payment_date <= ?2 order by payment_date asc", nativeQuery = true)
    List<Payment> findPaymentByDates(Date fromDate, Date toDate);

    @Query("select coalesce(sum(p.paymentAmount),0)  from Payment  p where p.month = :month and p.year = :year")
    Double getPaymentAmount(@Param("month") int month, @Param("year") int year);
}

