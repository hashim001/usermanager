package com.hellokoding.account.repository;

import com.hellokoding.account.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.sql.Date;
import java.util.List;
import org.springframework.data.jpa.repository.Modifying;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    public Employee findByCnic(String cnic);

    public Employee findByEmployeeId(int employee_id);

    List<Employee> findEmployeeByDeletedFalse();

    Employee findByNameAndDeletedFalse(String name);

    @Query("Select coalesce(sum(e.balance),0) from Employee e where e.isActive = 1 and e.deleted = false")
    Double getBalanceAmount();

    @Query("SELECT e FROM Employee e ")
    Page<Employee> findAllPaged(Pageable pageable);
}
