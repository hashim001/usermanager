/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hellokoding.account.repository;

import com.hellokoding.account.model.Employee;
import com.hellokoding.account.model.MonthlyPayment;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author muhammad
 */
public interface MonthlyPaymentRepository extends JpaRepository<MonthlyPayment, Long>  {
    public List<MonthlyPayment> findByEmployees(Employee employees);
}
