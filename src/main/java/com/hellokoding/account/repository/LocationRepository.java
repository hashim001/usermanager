package com.hellokoding.account.repository;

import com.hellokoding.account.model.Location;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface LocationRepository extends JpaRepository<Location, Long> {

    Location findByLocationId(int locationId);

    @Modifying
    @Transactional
    @Query("DELETE FROM Location l WHERE l.locationId= :locationId")
    void removeLocation(@Param("locationId") int locationId);

    List<Location> findAllByOrderByLocationNameAsc();

    @Query("SELECT l FROM Location l ")
    Page<Location> findAllPaged( Pageable pageable);
}
