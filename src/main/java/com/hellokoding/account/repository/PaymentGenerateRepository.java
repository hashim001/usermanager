/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hellokoding.account.repository;

import com.hellokoding.account.model.PaymentGenerate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author muhammad
 */
public interface PaymentGenerateRepository extends JpaRepository<PaymentGenerate, Long> {

    @Query("Select e from PaymentGenerate e  where e.month = :Month and e.year = :Year")
    List<PaymentGenerate> findByPaymentMonth(@Param("Month") String Month, @Param("Year") int Year);
    
}
