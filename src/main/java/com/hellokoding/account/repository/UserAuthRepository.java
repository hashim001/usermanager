package com.hellokoding.account.repository;

import com.hellokoding.account.model.UserAuth;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserAuthRepository extends JpaRepository<UserAuth, Long>{

    UserAuth findByUserName(String username);

    @Query("select u from UserAuth u where u.auth_id =:Authid ")
    UserAuth findByAuthId(@Param("Authid") int authid);
}
