package com.hellokoding.account.web;

import com.hellokoding.account.constants.Constants;
import com.hellokoding.account.model.Employee;
import com.hellokoding.account.model.Location;
import com.hellokoding.account.model.Payment;
import com.hellokoding.account.model.UserAuth;
import com.hellokoding.account.service.LocationService;
import com.hellokoding.account.service.Payment.PaymentGenerateService;
import com.hellokoding.account.service.Payment.PaymentService;
import com.hellokoding.account.service.datalisting.JSONPopulateService;
import com.hellokoding.account.service.user.RegistrationService;
import com.hellokoding.account.service.user.UserAuthService;
import com.hellokoding.account.validator.CredentialValidator;
import com.hellokoding.account.validator.UserDetailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import static java.time.temporal.TemporalAdjusters.firstDayOfYear;

import java.time.LocalDate;
import java.util.*;

@Controller
@SessionAttributes({"updateDetailForm", "packagePriceList", "employeesList"})
public class RegisterController {

    @Autowired
    private UserAuthService userAuthService;
    @Autowired
    private RegistrationService registrationService;
    @Autowired
    private CredentialValidator credentialValidator;

    @Autowired
    private JSONPopulateService jsonPopulateService;

    @Autowired
    private PaymentGenerateService paymentGenerateService;

    @Autowired
    private PaymentService paymentService;
    @Autowired
    private LocationService locationService;

    @RequestMapping(value = "/home", method = RequestMethod.GET)

    public String view_employees(Map<String, Object> model, RedirectAttributes attributes) {

        if (!paymentGenerateService.checkPaymentGenerate()) {
            model.put("paymentMessage", Constants.PAYMENT_MESSAGE);
        }
        LocalDate currentDate = LocalDate.now();
        List<Integer> yearList = new ArrayList<>();
        for (int year = 2018; year <= 2050; year++) {
            yearList.add(year);
        }
        model.put("detailForm", new Employee());
        model.put("paymentForm", new Payment());
        model.put("currentDate", currentDate);
        model.put("currentMonth", currentDate.getMonthValue());
        model.put("currentYear", currentDate.getYear());
        model.put("employee", registrationService.findAll());
        model.put("monthMap", paymentService.getMonthMap());
        model.put("yearList", yearList);
        model.put("balanceAmount", registrationService.getBalanceAmount());
        model.put("monthlyAmount", paymentService.getMonthlyPayment(currentDate.getMonthValue(), currentDate.getYear()));
        model.put("fromDate", LocalDate.of(2019,1,1));
        List<Location> locations = locationService.findAll();
        model.put("locationList", locations);
        return "employee";
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String registration(Model model) {

        model.addAttribute("credintialForm", new UserAuth());

        return "register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registration(@ModelAttribute("credintialForm") UserAuth userForm, BindingResult bindingResult, Model model) {
        credentialValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "register";
        }

        userAuthService.save(userForm);

        //   securityService.autologin(userForm.getUserName(), userForm.getPasswordConfirm());
        return "register";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        if (error != null) {
            model.addAttribute("error", "Your username and password is invalid.");
        }

        if (logout != null) {
            model.addAttribute("message", "You have been logged out successfully.");
        }

        return "login";
    }

    //Logout mapping
    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public String showLoggedout() {
        return "logout";
    }

    /**
     * Edit employee
     *
     * @param employeeId
     * @return
     */
    @RequestMapping(value = "/editEmployee", method = RequestMethod.GET)
    @ResponseBody
    public HttpEntity<Employee> editEmployee(@RequestParam("employeeId") int employeeId) {
        Employee employee = registrationService.findByEmployeeId(employeeId);
        employee.setLocationId(employee.getLocation().getLocationId());
        employee.setMonthlyPayments(null);
        employee.setPayments(null);
        employee.setPackagePrices(null);
        employee.setLocation(null);
        return new ResponseEntity(employee, HttpStatus.OK);
    }

    /**
     * upload Profile Pic
     *
     * @param attributes
     * @return home page
     */
    @RequestMapping(value = "/uploadProfilePic", method = RequestMethod.POST)
    public String uploadProfilePic(RedirectAttributes attributes, @RequestParam("urlPath") String urlPath,
                                   @RequestParam("employeeId") int employeeId,
                                   @RequestParam("profileImage") MultipartFile multipartFile) {
        Employee employee = registrationService.findByEmployeeId(employeeId);
        if (multipartFile.getSize() != 0) {
            registrationService.uploadImage(multipartFile);
            employee.setImageUrl(urlPath + "/" + multipartFile.getOriginalFilename());
            registrationService.save(employee);
        }
        attributes.addFlashAttribute("categoryRemoveSuccess", " <div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Stock Category removed successfully.</div>");
        return "redirect:/home";
    }
    @RequestMapping(value = "/Employee/json", method = RequestMethod.GET)
    @ResponseBody
    public HttpEntity<Page<Location>> employeeListingJSON(@RequestParam(name = "start", defaultValue = "0") int start
            , @RequestParam(name = "length", defaultValue = "10") int length
            , @RequestParam(name = "draw") int draw
            , @RequestParam(name = "search[value]", defaultValue = "") String searchString) {


        int pageIndex = 0;
        if (start > 0) {
            pageIndex = start / length;
        }
        return new ResponseEntity(jsonPopulateService.populateEmployeeList(locationService.findAllPaged(pageIndex, length, "locationName", true ), draw,searchString), HttpStatus.OK);
    }
}
