package com.hellokoding.account.web;

import com.hellokoding.account.service.EmailService;
import com.hellokoding.account.service.dbBackupService.DBBackupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class DBBackupController {

    @Autowired
    private DBBackupService dbBackupService;

    @Autowired
    private EmailService emailService;

    @RequestMapping(value = "/dbBackUp", method = RequestMethod.GET)
    public String dbBackUp(RedirectAttributes attributes) throws javax.mail.MessagingException {

        emailService.doSendEmail(dbBackupService.backupdbtosql());
        attributes.addFlashAttribute("dbCreated", " <div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">DB Backup created Successfully.</div>");

        return "redirect:/home";
    }
}
