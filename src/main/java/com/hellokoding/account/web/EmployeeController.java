package com.hellokoding.account.web;

import com.hellokoding.account.model.*;
import com.hellokoding.account.service.LocationService;
import com.hellokoding.account.service.Payment.PackagePriceService;
import com.hellokoding.account.service.Payment.PaymentService;
import com.hellokoding.account.service.user.RegistrationService;
import com.hellokoding.account.validator.UserDetailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Map;

@Controller
@SessionAttributes({"salaryUpdateForm", "employeesList", "updateDetailForm"})
public class EmployeeController {

    @Autowired
    private PaymentService paymentService;
    @Autowired
    private RegistrationService registrationService;//used as employee service
    @Autowired
    private UserDetailValidator userDetailValidator;
    @Autowired
    private PackagePriceService packagePriceService;
    @Autowired
    private LocationService locationService;

    @RequestMapping(value = "/employee_details", method = RequestMethod.GET)
    public String employee_details(Model model, @RequestParam(value = "employeeid", defaultValue = "0") int employeeId, Map<String, Object> dmap, RedirectAttributes attributes) {

        Employee employee = registrationService.findByEmployeeId(employeeId);
        dmap.put("salary_script", paymentService.findAll(employee));
        dmap.put("employee_details", registrationService.findByEmployeeId(employeeId));
        model.addAttribute("employee_id", employeeId);

        return "employeedetail";

    }

    @RequestMapping(value = "/userdetails", method = RequestMethod.GET)
    public String detail_form(Model model) {

        model.addAttribute("currentDate", LocalDate.now());
        model.addAttribute("detailForm", new Employee());

        return "userdetailform";
    }

    @RequestMapping(value = "/userdetails", method = RequestMethod.POST)
    public String detail_form(@ModelAttribute("detailForm") Employee detailForm,
                              RedirectAttributes attributes) {

        if(registrationService.findByName(detailForm.getName()) != null && detailForm.getEmployeeId() == 0){
            attributes.addFlashAttribute("employeeRegisterSuccess", "<div style=\"background-color:red;height: 40px;text-align: center;padding-top: 5px;border: 2px solid red; color:white;border-radius: 5px;font-size: initial;\">User with Name " + detailForm.getName() + " has already been registered. Please enter unique name!</div>");
            return "redirect:/home";
        }
        String imageUrl = detailForm.getCincPath();
        if (detailForm.getCnicFront().getSize() != 0) {
            registrationService.uploadImage(detailForm.getCnicFront());
            detailForm.setCnicFrontUrl(imageUrl + "/" + detailForm.getCnicFront().getOriginalFilename());
        }
        if (detailForm.getCnicBack().getSize() != 0) {
            registrationService.uploadImage(detailForm.getCnicBack());
            detailForm.setCnicBackUrl(imageUrl + "/" + detailForm.getCnicBack().getOriginalFilename());
        }
        Location location = locationService.findByLocationId(detailForm.getLocationId());
        PackagePrice packagePrice = packagePriceService.findByPackageName(detailForm.getStatus());
        detailForm.setLocation(location);
        detailForm.setPackagePrices(packagePrice);
        registrationService.save(detailForm);

        // securityService.autologin(detailForm.getUserName(), detailForm.getPasswordConfirm());
        attributes.addFlashAttribute("employeeRegisterSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">User " + detailForm.getName() + " has been registered successfully.</div>");
        return "redirect:/home";
    }

    @RequestMapping(value = "/updateEmployee", method = RequestMethod.GET)
    public String update_user(Model model,
                              @RequestParam(value = "employeeid", defaultValue = "0") int employeeId
    ) {
        model.addAttribute("updateDetailForm", registrationService.findByEmployeeId(employeeId));

        return "userdetailupdate";
    }

    @RequestMapping(value = "/updateEmployee", method = RequestMethod.POST)
    public String update_form(@ModelAttribute("updateDetailForm") Employee detailForm,
                              RedirectAttributes attributes
    ) {
        String imageUrl = detailForm.getCincPath();
        if (detailForm.getCnicFront().getSize() != 0) {
            registrationService.uploadImage(detailForm.getCnicFront());
            detailForm.setCnicFrontUrl(imageUrl + "/" + detailForm.getCnicFront().getOriginalFilename());
        }
        if (detailForm.getCnicBack().getSize() != 0) {
            registrationService.uploadImage(detailForm.getCnicBack());
            detailForm.setCnicBackUrl(imageUrl + "/" + detailForm.getCnicBack().getOriginalFilename());
        }
        PackagePrice packagePrice = packagePriceService.findByPackageName(detailForm.getStatus());
        detailForm.setPackagePrices(packagePrice);

        registrationService.save(detailForm);

        attributes.addFlashAttribute("employeeUpdateSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Details for User: " + detailForm.getName() + " have been updated.</div>");
        return "redirect:/home";
    }

    /**
     * Delete Employee
     *
     * @param attributes
     * @param employeeId
     * @return
     */
    @RequestMapping(value = "/removeEmployee", method = RequestMethod.POST)
    public String removeEmployee(RedirectAttributes attributes, @RequestParam("employeeId") int employeeId) {

        Employee employee = registrationService.findByEmployeeId(employeeId);
        employee.setDeleted(true);
        registrationService.save(employee);
        attributes.addFlashAttribute("employeeRegisterSuccess", " <div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Employee removed successfully.</div>");

        return "redirect:/home";
    }

}
