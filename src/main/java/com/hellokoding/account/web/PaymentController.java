package com.hellokoding.account.web;

import com.hellokoding.account.model.*;
import com.hellokoding.account.service.Payment.MonthlyPaymentService;
import com.hellokoding.account.service.Payment.PackagePriceService;
import com.hellokoding.account.service.Payment.PaymentGenerateService;
import com.hellokoding.account.service.Payment.PaymentService;
import com.hellokoding.account.service.user.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.*;

@Controller
@SessionAttributes("packagePriceList")
public class PaymentController {
    @Autowired
    private RegistrationService registrationService;

    @Autowired
    private MonthlyPaymentService monthlyPaymentService;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private PaymentGenerateService paymentGenerateService;

    @Autowired
    private PackagePriceService packagePriceService;

    @RequestMapping(value = "/paymentdetails/addPayment", method = RequestMethod.GET)
    public String addPayment(Model model) {

        LocalDate currentDate = LocalDate.now();
        List<Integer> yearList = new ArrayList<>();
        for (int year = 2018; year <= 2050; year++) {
            yearList.add(year);
        }

        model.addAttribute("paymentForm", new Payment());

        model.addAttribute("monthMap", paymentService.getMonthMap());
        model.addAttribute("yearList", yearList);
        model.addAttribute("currentDate", currentDate);
        model.addAttribute("currentMonth", currentDate.getMonthValue());
        model.addAttribute("currentYear", currentDate.getYear());
        model.addAttribute("employeeList", registrationService.findAll());
        model.addAttribute("paymentList", paymentService.findAllPayments());
        return "payment";
    }

    @RequestMapping(value = "/paymentdetails/addPayment", method = RequestMethod.POST)
    public String addPayment_post(@ModelAttribute("paymentForm") Payment payment, RedirectAttributes attributes) {
        if (payment.getPaymentId() > 0) {
            Employee editemployee = registrationService.findByEmployeeId(payment.getPreviousEmployee());
            editemployee.setBalance(editemployee.getBalance() + payment.getPreviousAmount());
            registrationService.save(editemployee);
        }
        Employee employee = registrationService.findByEmployeeId(payment.getEmployeeId());
        employee.setBalance(employee.getBalance() - payment.getPaymentAmount());
        employee.setPaid(true);
        registrationService.save(employee);
        payment.setEmployees(employee);
        paymentService.save(payment);
        if (payment.getPaymentId() > 0) {
            attributes.addFlashAttribute("paymentSuccess", " <div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Payment updated successfully.</div>");
            return "redirect:/paymentdetails/addPayment";
        }
        attributes.addFlashAttribute("paymentSuccess", " <div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Payment added successfully.</div>");
        return "redirect:/home";

    }

    /**
     * Edit payment
     *
     * @param paymentId
     * @return
     */
    @RequestMapping(value = "/paymentdetails/ajax/editPayment", method = RequestMethod.GET)
    @ResponseBody
    public HttpEntity<Payment> paymentForm(@RequestParam("paymentId") int paymentId) {
        Payment payment = paymentService.findByPaymentId(paymentId);
        payment.setEmployeeId(payment.getEmployees().getEmployeeId());
        payment.setEmployees(null);
        return new ResponseEntity(payment, HttpStatus.OK);
    }

    /**
     * Delete payment
     *
     * @param attributes
     * @param paymentId
     * @return
     */
    @RequestMapping(value = "/paymentdetails/removePayment", method = RequestMethod.POST)
    public String removePayment(RedirectAttributes attributes, @RequestParam("paymentId") int paymentId) {

        Payment payment = paymentService.findByPaymentId(paymentId);

        Employee employee = registrationService.findByEmployeeId(payment.getEmployees().getEmployeeId());
        employee.setBalance(employee.getBalance() + payment.getPaymentAmount());
        employee.setPaid(false);
        registrationService.save(employee);
        paymentService.removePayment(payment);
        attributes.addFlashAttribute("PaymentRemoveSuccess", " <div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Payment removed successfully.</div>");

        return "redirect:/paymentdetails/addPayment";
    }

/*
    @RequestMapping(value = "/paymentgenerate", method = RequestMethod.GET)
    public ModelAndView payment_generate(Model model) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        int monthInt = calendar.get(Calendar.MONTH);//salary calculated in next month
        int year = calendar.get(Calendar.YEAR);
        String month = paymentService.getMonthName(monthInt);
        model.addAttribute("monthList", month);
        model.addAttribute("yearList", Integer.toString(year));
        model.addAttribute("current_month", month);
        model.addAttribute("current_year", year);
        EmployeeList employeeList = new EmployeeList();
        employeeList.setEmployees(registrationService.findAll());
        return new ModelAndView("paymentgenerate", "employeesList", employeeList);
    }
*/


    @RequestMapping(value = "/paymentgenerate", method = RequestMethod.GET)
    public String payment_generate(RedirectAttributes attributes) {
        Calendar calendar = Calendar.getInstance();
        int monthInt = calendar.get(Calendar.MONTH);//salary calculated in next month
        int year = calendar.get(Calendar.YEAR);
        String month = paymentService.getMonthName(monthInt);
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        List<PaymentGenerate> paymentGenerateList = paymentGenerateService.findByMonthAndYear(month, year);
        List<Employee> employeeList = registrationService.findAll();
        if (paymentGenerateList.size() > 0) {
            System.out.println("Already generated");
            attributes.addFlashAttribute("paymentGenerationNotSuccess", "<div style=\"background-color: red ;height: 40px;text-align: center;padding-top: 5px;border: 2px solid red; color:white;border-radius: 5px;font-size: initial;\">Payment Already Added For this Month.</div>");
            return "redirect:/home";
        } else {
            for (Employee employee : employeeList ) {

                if (employee.getIsActive() == 0) {
                    continue;
                }

               // double monthlyFee = registrationService.setPackageRate(employee.getStatus());
                double monthlyFee = employee.getMonthlyAmount();
                monthlyFee = employee.getBalance() + monthlyFee;
                employee.setBalance(monthlyFee);
                employee.setPaid(false);
                registrationService.save(employee);
            }

            PaymentGenerate paymentGenerate = new PaymentGenerate();
            paymentGenerate.setMonth(month);
            paymentGenerate.setYear(year);
            paymentGenerate.setDate(date.toString());
            paymentGenerateService.save(paymentGenerate);

            paymentGenerateList = paymentGenerateService.findByMonthAndYear(month, year);
            for (Employee employee : employeeList) {
                if (employee.getIsActive() == 0) {
                    continue;
                }
                MonthlyPayment monthlyPayment = new MonthlyPayment();
                monthlyPayment.setEmployees(employee);
              //  double monthlyFee = registrationService.setPackageRate(employee.getStatus());
                monthlyPayment.setPaymentRate(employee.getMonthlyAmount());
                monthlyPayment.setDate(date);
                monthlyPayment.setPaymentGenerates(paymentGenerateList.get(0));
                monthlyPaymentService.save(monthlyPayment);
            }
            attributes.addFlashAttribute("paymentGenerationSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Payment added successfully.</div>");
            System.out.println("Successfully generated");

            return "redirect:/home";
        }
    }

    @RequestMapping(value = "/packagerates", method = RequestMethod.GET)
    public String package_rates(Model model) {

        PackagePriceList packagePriceList = new PackagePriceList();
        packagePriceList.setPackagePrices(packagePriceService.findAll());
        model.addAttribute("prices", paymentService.prices());
        model.addAttribute("packagePriceList", packagePriceList);
        return "packagerates";
    }

    @RequestMapping(value = "/packagerates", method = RequestMethod.POST)
    public String package_rates(@ModelAttribute("packagePriceList") PackagePriceList packagePriceList, BindingResult bindingResult,
                                RedirectAttributes attributes
    ) {

        for (PackagePrice packagePrice : packagePriceList.getPackagePrices()) {
            packagePriceService.save(packagePrice);
        }

        attributes.addFlashAttribute("packagePriceUpdated", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Package Price has been updated.</div>");
        return "redirect:/home";
    }

    @RequestMapping(value = "/paymentgenerateforsingleuser", method = RequestMethod.GET)
    public ModelAndView paymentGenerateForSingleUser(Model model, @RequestParam(value = "employeeid", defaultValue = "0") int employeeId, RedirectAttributes attributes) {
        Calendar calendar = Calendar.getInstance();
        int monthInt = calendar.get(Calendar.MONTH);//salary calculated in next month
        int year = calendar.get(Calendar.YEAR);
        String month = paymentService.getMonthName(monthInt);
        model.addAttribute("monthList", month);
        model.addAttribute("yearList", Integer.toString(year));
        model.addAttribute("current_month", month);
        model.addAttribute("current_year", year);
        EmployeeList employeeList = new EmployeeList();
        employeeList.setEmployees(registrationService.findAll());
        return new ModelAndView("paymentgenerate", "employeesList", employeeList);
    }
}
