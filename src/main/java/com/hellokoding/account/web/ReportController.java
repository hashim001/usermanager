package com.hellokoding.account.web;

import com.hellokoding.account.model.Payment;
import com.hellokoding.account.service.Payment.PaymentService;
import com.hellokoding.account.service.user.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.DateFormatSymbols;
import java.util.*;
import java.sql.Date;

@Controller
public class ReportController {

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private RegistrationService registrationService;

    @RequestMapping(value = "/Reports/paymentReport", method = RequestMethod.GET)
    public String paymentReport(Model model, @RequestParam("fromDate") Date fromDate
            , @RequestParam("toDate") Date toDate, @RequestParam("employeeId") int employeeId) {

        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());

        model.addAttribute("currentDate", date);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("currentEmployee", employeeId);
        List<Integer> yearList = new ArrayList<>();
        for (int intYear = 2018; intYear <= 2050; intYear++) {
            yearList.add(intYear);
        }
        model.addAttribute("monthMap", paymentService.getMonthMap());
        model.addAttribute("yearList", yearList);
        List<Payment> payments;
        if (employeeId > 0) {
            payments = paymentService.findPaymentByDatesAndEmployee(fromDate, toDate, employeeId);
        } else {
            payments = paymentService.findPaymentByDates(fromDate, toDate);
        }
        model.addAttribute("payments", payments);
        model.addAttribute("employeeList", registrationService.findAll());


        return "reports/paymentReport";
    }
}
