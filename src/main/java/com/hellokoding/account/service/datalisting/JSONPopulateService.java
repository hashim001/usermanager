package com.hellokoding.account.service.datalisting;

import com.hellokoding.account.model.Employee;
import com.hellokoding.account.model.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

 import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static java.lang.Math.toIntExact;
import static java.time.temporal.TemporalAdjusters.firstDayOfYear;

@Service
public class JSONPopulateService {


    /**
     * Method to get Project Name for resource mapping.
     * Alternate for @Resource
     *
     * @return Project name String
     */

    public String getProjectName() {
        return "";
    }

    /**
     * employee Listing Populate
     *
     * @param locationPage
     * @param draw
     * @return
     */
    public JSONDataModel populateEmployeeList(Page<Location> locationPage, int draw, String search) {

        JSONDataModel JSONDataModel = new JSONDataModel();
        JSONDataModel.setDraw(draw);
        JSONDataModel.setRecordsTotal(toIntExact(locationPage.getTotalElements()));
        JSONDataModel.setRecordsFiltered(toIntExact(locationPage.getTotalElements()));
        List<String[]> dataString = new ArrayList<>();
        LocalDate fromDate = LocalDate.now().with(firstDayOfYear());
        LocalDate currentDate = LocalDate.now();
        for (Location location : locationPage.getContent()) {

            dataString.add(new String[]{
                    "<h3><u>" + location.getLocationName() + "</u></h3>",
                    "", "", "", "", "", "", ""

            });

            String imageUrl;
            String activeStatus;
            String paidStatus;
            Collections.sort(location.getEmployees(), Comparator.comparing(Employee::getName));
            for (Employee employee : location.getEmployees()) {
                if (!employee.isDeleted() && employee.getName().contains(search)) {
                    if (employee.getImageUrl() == null || employee.getImageUrl().equals("")) {
                        imageUrl = "No image";
                    } else {
                        imageUrl = "  <img src=\"" + employee.getImageUrl() + "\" id=\"myImg" + employee.getEmployeeId() + "\"\n" +
                                "                                                         class=\"myImg rotate90\" onclick=\"modalOpen( " + employee.getEmployeeId() + ");\"\n" +
                                "                                                         alt=\"" + employee.getName() + "\" height=\"20\" width=\"20\"/>";
                    }
                    if (employee.getIsActive() == 1) {
                        activeStatus = "<img src=\"" + getProjectName() + "/resources/images/active.png\" height=\"32\"\n" +
                                "                                                         width=\"32\"/>";
                    } else {
                        activeStatus = "<img src=\"" + getProjectName() + "/resources/images/inactive.png\" height=\"32\"\n" +
                                "                                                         width=\"32\"/>";
                    }
                    if (employee.isPaid()) {
                        paidStatus = "<img src=\"" + getProjectName() + "/resources/images/thumbs-up.png\" height=\"32\"\n" +
                                "                                                         width=\"32\"/>";
                    } else {
                        paidStatus = "<img src=\"" + getProjectName() + "/resources/images/thumbs-down.png\" height=\"32\"\n" +
                                "                                                         width=\"32\"/>";
                    }
                    dataString.add(new String[]{
                            " <a href=\"" + getProjectName() + "/Reports/paymentReport?employeeId=" + employee.getEmployeeId() + "&fromDate=" + fromDate + "&toDate=" + currentDate + "\"\n" +
                                    " style=\"text-transform: capitalize\"><h4>" + employee.getName() + "</h4></a>",
                            imageUrl, activeStatus, employee.getStatus(), employee.getPhone(), Double.toString(employee.getBalance()), paidStatus,
                            "             <form method=\"post\" id=\"uploadImage" + employee.getEmployeeId() + "\"\n" +
                                    "                                                  enctype=\"multipart/form-data\"\n" +
                                    "                                                  action=\"" + getProjectName() + "/uploadProfilePic\">\n" +
                                    "\n" +
                                    "                                                <input type=\"hidden\" name=\"urlPath\"\n" +
                                    "                                                       value=\"" + getProjectName() + "/resources/images\"/>\n" +
                                    "                                                <input type=\"hidden\" name=\"employeeId\" value=\"" + employee.getEmployeeId() + "\"/>\n" +
                                    "                                                <input type=\"file\" name=\"profileImage\" id=\"originalFile" + employee.getEmployeeId() + "\"\n" +
                                    "                                                       onchange=\"uploadImage(" + employee.getEmployeeId() + ");\"/>\n" +
                                    "                                            </form>\n" +
                                    "                                            <form method=\"post\" id=\"employeeRemove" + employee.getEmployeeId() + "\"\n" +
                                    "                                                  action=\"" + getProjectName() + "/removeEmployee\">\n" +
                                    "\n" +
                                    "                                                <input type=\"hidden\" name=\"employeeId\"\n" +
                                    "                                                       value=\"" + employee.getEmployeeId() + "\"/>\n" +
                                    "                                            </form>\n" +
                                    "                                            <ul class=\"list-inline\">\n" +
                                    "                                                <li><a title=\"payment\" href=\"#\"\n" +
                                    "                                                       onclick=\"editPaymentForm(" + employee.getEmployeeId() + ")\"><img\n" +
                                    "                                                        src=\"" + getProjectName() + "/resources/images/bill.png\" alt=\"\"></a></li>\n" +
                                    "                                                <li><a title=\"edit\" href=\"#\" onclick=\"edit(" + employee.getEmployeeId() + ")\"\n" +
                                    "                                                       class=\"editForm\"><img\n" +
                                    "                                                        src=\"" + getProjectName() + "/resources/images/edit.png\" alt=\"\"></a></li>\n" +
                                    "                                                <li><a title=\"delete\" href=\"#\" onclick=\"remove(" + employee.getEmployeeId() + ")\"><img\n" +
                                    "                                                        src=\"" + getProjectName() + "/resources/images/remove.png\" alt=\"\"></a>\n" +
                                    "                                                </li>\n" +
                                    "\n" +
                                    "                                            </ul>"

                    });
                }

            }

        }
        JSONDataModel.setData(dataString);

        return JSONDataModel;
    }
}
