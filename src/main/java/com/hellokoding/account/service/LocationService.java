package com.hellokoding.account.service;

import com.hellokoding.account.model.Location;
import com.hellokoding.account.repository.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocationService {

    @Autowired
    private LocationRepository locationRepository;

    public Location findByLocationId(int locationId) {
        return locationRepository.findByLocationId(locationId);
    }

    public List<Location> findAll() {
        return locationRepository.findAllByOrderByLocationNameAsc();
    }

    public void save(Location location) {
        locationRepository.save(location);
    }

    public void remove(int locationId) {
        locationRepository.removeLocation(locationId);
    }
    public Page<Location> findAllPaged(int pageIndex, int size, String orderColumn, boolean isAsc ) {
        Sort sort;
        if (isAsc) {
            sort = new Sort(Sort.Direction.ASC, orderColumn);
        } else {
            sort = new Sort(Sort.Direction.DESC, orderColumn);
        }
        return locationRepository.findAllPaged( new PageRequest(pageIndex, size, sort));
    }
}
