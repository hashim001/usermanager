/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hellokoding.account.service.Payment;

import com.hellokoding.account.model.PaymentGenerate;
import com.hellokoding.account.repository.PaymentGenerateRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author muhammad
 */
@Service
public class PaymentGenerateServiceImpl implements PaymentGenerateService {

    @Autowired
    PaymentGenerateRepository paymentGenerateRepository;
    @Autowired
    PaymentService paymentService;

    public void save(PaymentGenerate paymentGenerate) {
        paymentGenerateRepository.save(paymentGenerate);
    }

    @Override
    public List<PaymentGenerate> findByMonthAndYear(String month, int year) {
        return paymentGenerateRepository.findByPaymentMonth(month, year);
    }

    @Override
    public boolean checkPaymentGenerate() {
        boolean check = false;

        Calendar calendar = Calendar.getInstance();
        int monthInt = calendar.get(Calendar.MONTH);//salary calculated in next month
        int year = calendar.get(Calendar.YEAR);
        String month = paymentService.getMonthName(monthInt);
        List<PaymentGenerate> paymentGenerateList;
        paymentGenerateList = paymentGenerateRepository.findByPaymentMonth(month, year);
        if (paymentGenerateList.size() >= 1 ) {
            check = true;
        }
         return check;
    }

}
