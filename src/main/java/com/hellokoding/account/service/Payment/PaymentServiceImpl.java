/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hellokoding.account.service.Payment;

import com.hellokoding.account.model.Employee;
import com.hellokoding.account.model.Payment;
import com.hellokoding.account.model.PaymentGenerate;
import com.hellokoding.account.model.PaymentList;
import com.hellokoding.account.repository.PaymentRepository;

import java.sql.Date;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author muhammad
 */
@Service
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    private PaymentRepository paymentRepository;

    @Override
    public void save(Payment payment) {
        paymentRepository.save(payment);
    }

    @Transactional
    @Override
    public List<Payment> findByMonthAndYear(String month, int year, Employee employee) {
        return paymentRepository.findByPaymentMonth(month, year, employee);
    }

    /*   @Transactional
       @Override
       public void updatePayment(String month, int year, int paymentId, String date) {
           paymentRepository.updatePayment(month, year, paymentId, date);
       }
   */
    @Override
    public List<Payment> findAll(Employee employees) {
        return paymentRepository.findByEmployees(employees);
    }

    public List<Integer> prices() {
        List<Integer> priceList = new ArrayList<>();
        for (int i = 850; i <= 5000; i += 50) {
            priceList.add(i);
        }

        return priceList;
    }

    @Override
    public Payment findByPaymentId(int paymentId) {
        return paymentRepository.findByPaymentId(paymentId);
    }

    public String getMonthName(int monthInt) {

        String month = "wrong";
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getMonths();
        if (monthInt >= 0 && monthInt <= 11) {
            month = months[monthInt];
        }
        return month;
    }

    public List<String> getMonthList() {
        List<String> monthList = new ArrayList<>();
        monthList.add("January");
        monthList.add("February");
        monthList.add("March");
        monthList.add("April");
        monthList.add("May");
        monthList.add("June");
        monthList.add("July");
        monthList.add("August");
        monthList.add("September");
        monthList.add("October");
        monthList.add("November");
        monthList.add("December");
        return monthList;

    }

    public List<Integer> findDistinctDay() {
        List<Integer> distinctDays = new ArrayList<>();
        for (int i = 1; i <= 31; i++) {
            distinctDays.add(i);
        }

        return distinctDays;
    }

    @Override
    public List<Payment> findAllPayments() {
        return paymentRepository.findAll();
    }

    @Override
    public void removePayment(Payment payment) {
        paymentRepository.delete(payment);
    }

    @Override
    public Map getMonthMap() {
        Map monthMap = new HashMap();
        monthMap.put(1, "January");
        monthMap.put(2, "Febraury");
        monthMap.put(3, "March");
        monthMap.put(4, "April");
        monthMap.put(5, "May");
        monthMap.put(6, "June");
        monthMap.put(7, "July");
        monthMap.put(8, "August");
        monthMap.put(9, "September");
        monthMap.put(10, "October");
        monthMap.put(11, "November");
        monthMap.put(12, "December");
        return monthMap;
    }

    @Override
    public List<Payment> findPaymentByMonthAndYearAndEmployee(int fromMonth, int toMonth, int year, int employeeId) {
        return paymentRepository.findPaymentByMonthAndYearAndEmployee(fromMonth, toMonth, year, employeeId);
    }

    @Override
    public List<Payment> findPaymentByMonthAndYear(int fromMonth, int toMonth, int year) {
        return paymentRepository.findPaymentByMonthAndYear(fromMonth, toMonth, year);
    }

    @Override
    public List<Payment> findPaymentByDatesAndEmployee(Date fromDate, Date toDate, int employeeId) {
        return paymentRepository.findPaymentByDatesAndEmployee(fromDate, toDate, employeeId);
    }

    @Override
    public List<Payment> findPaymentByDates(Date fromDate, Date toDate) {
        return paymentRepository.findPaymentByDates(fromDate, toDate);
    }

    //Return the Month Number
    public int getMonthInt(String month) {

        int monthNumber = 0;
        switch (month) {
            case "January":
                monthNumber = 1;
                break;
            case "February":
                monthNumber = 2;
                break;
            case "March":
                monthNumber = 3;
                break;
            case "April":
                monthNumber = 4;
                break;
            case "May":
                monthNumber = 5;
                break;
            case "June":
                monthNumber = 6;
                break;
            case "July":
                monthNumber = 7;
                break;
            case "August":
                monthNumber = 8;
                break;
            case "September":
                monthNumber = 9;
                break;
            case "October":
                monthNumber = 10;
                break;
            case "November":
                monthNumber = 11;
                break;
            case "December":
                monthNumber = 12;
                break;
        }
        return monthNumber;
    }

    @Override
    public double getMonthlyPayment(int month, int year) {
        return paymentRepository.getPaymentAmount(month, year);
    }
}
