/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hellokoding.account.service.Payment;

import com.hellokoding.account.model.PackagePrice;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author muhammad
 */
@Service
public interface PackagePriceService {

    public PackagePrice findByPackageName(String packageName);
    List<PackagePrice> findAll();
    public void save(PackagePrice packagePrice);
}
