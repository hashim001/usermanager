/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hellokoding.account.service.Payment;

import com.hellokoding.account.model.Employee;
import com.hellokoding.account.model.Payment;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

/**
 * @author muhammad
 */
@Service
public interface PaymentService {

    void save(Payment payment);

    List<Payment> findAll(Employee employees);

    List<Payment> findByMonthAndYear(String month, int year, Employee employee);

/*
     void updatePayment(String month, int year, int paymentId, String date);
*/

    List<Integer> prices();

    String getMonthName(int monthInt);

    List<Integer> findDistinctDay();

    Payment findByPaymentId(int paymentId);

    List<Payment> findAllPayments();

    void removePayment(Payment payment);

    Map getMonthMap();

    List<Payment> findPaymentByMonthAndYearAndEmployee(int fromMonth,int toMonth,int year,int employeeId);

    List<Payment> findPaymentByDatesAndEmployee(Date fromDate, Date toDate, int employeeId);

    List<Payment> findPaymentByDates(Date fromDate, Date toDate);

    List<Payment> findPaymentByMonthAndYear(int fromMonth,int toMonth,int year);

    int getMonthInt(String month);

    double getMonthlyPayment(int month,int year);
}
