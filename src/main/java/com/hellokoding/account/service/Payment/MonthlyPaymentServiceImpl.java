/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hellokoding.account.service.Payment;

import com.hellokoding.account.model.Employee;
import com.hellokoding.account.model.MonthlyPayment;
import com.hellokoding.account.repository.MonthlyPaymentRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author muhammad
 */
@Service
public class MonthlyPaymentServiceImpl implements MonthlyPaymentService {

    @Autowired
    MonthlyPaymentRepository monthlyPaymentRepository;

    public void save(MonthlyPayment monthlyPayment) {
        monthlyPaymentRepository.save(monthlyPayment);

    }

    public void delete(MonthlyPayment monthlyPayment) {
        monthlyPaymentRepository.delete(monthlyPayment);

    }

    public List<MonthlyPayment> findByEmployeeId(Employee employees) {
        return monthlyPaymentRepository.findByEmployees(employees);
    }

}
