/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hellokoding.account.service.Payment;

import com.hellokoding.account.model.PackagePrice;
import com.hellokoding.account.repository.PackagePriceRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author muhammad
 */
@Service
public class PackagePriceServiceImpl implements PackagePriceService {

    @Autowired
    PackagePriceRepository packagePriceRepository;

    @Override
    public PackagePrice findByPackageName(String packageName) {

        return packagePriceRepository.findByPackageName(packageName);
    }

    @Override
    public void save(PackagePrice packagePrice) {
        packagePriceRepository.save(packagePrice);
    }

    @Override
    public List<PackagePrice> findAll() {
        return packagePriceRepository.findAll();
    }
}
