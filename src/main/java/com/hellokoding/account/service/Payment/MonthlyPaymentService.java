/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hellokoding.account.service.Payment;

import com.hellokoding.account.model.Employee;
import com.hellokoding.account.model.MonthlyPayment;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author muhammad
 */
@Service
public interface MonthlyPaymentService {

    public void save(MonthlyPayment monthlyPayment);

    public void delete(MonthlyPayment monthlyPayment);

    public List<MonthlyPayment> findByEmployeeId(Employee employees);
}
