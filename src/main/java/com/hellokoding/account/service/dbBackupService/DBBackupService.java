package com.hellokoding.account.service.dbBackupService;

import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;

@Service
public class DBBackupService {

    public String backupdbtosql() {
        String savePath = "";
        try {
             String dir =  "C:";

            /*NOTE: Creating Database Constraints*/
            String dbName = "employeemanager";
            String dbUser = "root";

            /*NOTE: Creating Path Constraints for folder saving*/
            /*NOTE: Here the backup folder is created for saving inside it*/
            String folderPath = dir + "/backup";

            /*NOTE: Creating Folder if it does not exist*/
            File f1 = new File(folderPath);
            f1.mkdir();

            /*NOTE: Creating Path Constraints for backup saving*/
            /*NOTE: Here the backup is saved in a folder called backup with the name backup.sql*/
            savePath =  dir + "/backup/" + LocalDate.now()+ "-backup.sql";
            System.out.println(savePath);
            /*NOTE: Used to create a cmd command*/
            String executeCmd = "mysqldump -u " + dbUser + " --add-drop-database -B " + dbName + " -r " + savePath;

            Process runtimeProcess = Runtime.getRuntime().exec(new String[] { "cmd.exe", "/c", executeCmd });
            int processComplete = runtimeProcess.waitFor();

            /*NOTE: processComplete=0 if correctly executed, will contain other values if not*/
            if (processComplete == 0) {
                System.out.println("Backup Complete");
            } else {
                System.out.println("Backup Failure");
            }

        } catch (IOException | InterruptedException ex) {
            ex.printStackTrace();
        }
        return savePath;
    }
}

