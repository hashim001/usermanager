package com.hellokoding.account.service.user;
import com.hellokoding.account.model.Employee;
import com.hellokoding.account.model.PackagePrice;
import com.hellokoding.account.repository.EmployeeRepository;
import com.hellokoding.account.service.Payment.PackagePriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import sun.awt.image.FileImageSource;
import sun.awt.image.JPEGImageDecoder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.List;
@Service
public class RegistrationServiceImpl implements RegistrationService {
    
    @Autowired
    EmployeeRepository employeeRepository;
    
    @Autowired
    private PackagePriceService packagePriceService;
    @Override
    public void save(Employee employee) {

        employeeRepository.save(employee);
    }
    
    @Override
    public List<Employee> findAll() {
        return employeeRepository.findEmployeeByDeletedFalse();
    }

    @Override
    public Employee findByCnic(String cnic) {
        return employeeRepository.findByCnic(cnic);
    }
    
    @Override
    public Employee findByEmployeeId(int employee_id) {
        
        return employeeRepository.findByEmployeeId(employee_id);
    }
    
    @Override
    public double setPackageRate(String packageName) {
        PackagePrice packagePriceObject = packagePriceService.findByPackageName(packageName);
        double packagePrice;
        packagePrice = packagePriceObject.getPackagePrice();
        
        return packagePrice;
        
    }

    /////////////////////////upload file function////////////////////////////////////
    public void uploadImage(MultipartFile multipartFile) {
        URL resource = getClass().getResource("/");
      //  String path = resource.getPath();
        //  path = path.replace("target/classes/", "src/main/webapp/resources/images/") + multipartFile.getOriginalFilename();
        // String path = "/account/resources/images/" + multipartFile.getOriginalFilename();
         String path =  "C:/Program Files/Apache Software Foundation/Tomcat 7.0/webapps/Connect/resources/images/" + multipartFile.getOriginalFilename();
       // String path =  "D:/employee_manager/usermanager/src/main/webapp/resources/images/" + multipartFile.getOriginalFilename();

        File file = new File(path);
        try {
            JPEGImageDecoder decoder = new JPEGImageDecoder(new FileImageSource(file.getAbsolutePath()), new FileInputStream(file.getAbsolutePath()));
            decoder.produceImage();

        } catch (Exception e) {
            try {
                file.exists();
                file.delete();
                OutputStream os = new FileOutputStream(path);
                byte[] imageByteArray = multipartFile.getBytes();
                os.write(imageByteArray);
                os.flush();
                os.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public Employee findByName(String name) {
        return employeeRepository.findByNameAndDeletedFalse(name);
    }

    @Override
    public double getBalanceAmount() {
        return employeeRepository.getBalanceAmount();
    }

    public Page<Employee> findAllPaged(int pageIndex, int size, String orderColumn, boolean isAsc ) {
        Sort sort;
        if (isAsc) {
            sort = new Sort(Sort.Direction.ASC, orderColumn);
        } else {
            sort = new Sort(Sort.Direction.DESC, orderColumn);
        }
        return employeeRepository.findAllPaged( new PageRequest(pageIndex, size, sort));
    }
}
