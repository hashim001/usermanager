package com.hellokoding.account.service.user;

import com.hellokoding.account.model.UserAuth;
import com.hellokoding.account.repository.UserAuthRepository;

public interface UserAuthService{

   void save(UserAuth user);
    public UserAuth findByUserName(String username);

}
