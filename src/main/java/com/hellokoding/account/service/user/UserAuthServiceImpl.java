package com.hellokoding.account.service.user;


        import com.hellokoding.account.model.UserAuth;
        import com.hellokoding.account.repository.UserAuthRepository;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
        import org.springframework.stereotype.Service;

@Service
public class UserAuthServiceImpl implements UserAuthService{


    @Autowired
    private UserAuthRepository userAuthRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void save(UserAuth user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setUserId("3");
        user.setUserType("monga");
        userAuthRepository.save(user);
    }

    @Override
    public UserAuth findByUserName(String username) {
        return userAuthRepository.findByUserName(username);
    }
}
