package com.hellokoding.account.service.user;


import com.hellokoding.account.model.Employee;
import org.springframework.data.repository.query.Param;
import org.springframework.web.multipart.MultipartFile;

import java.sql.Date;
import java.util.List;


public interface RegistrationService {
    void save(Employee user);
    public Employee findByCnic(String cnic);
    List<Employee> findAll();
    public Employee findByEmployeeId(int employee_id);
    public double setPackageRate(String packageName);
    public void uploadImage(MultipartFile multipartFile);
    Employee findByName(String name);
    double getBalanceAmount();
}
