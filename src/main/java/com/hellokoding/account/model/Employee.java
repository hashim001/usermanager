package com.hellokoding.account.model;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.List;

@Entity
@Table(name = "employee")
public class Employee implements Serializable {

    @Id
    @Column(name = "employee_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int employeeId;
    @Column(name = "name")
    private String name;
    @Column(name = "status")
    private String status;
    private String cnic;
    @Column(name = "join_date")
    private Date joinDate;
    @Column(name = "balance")
    private double balance;
    private String address;
    private String phone;
    private int isActive;
    @Column(name = "cnicFront_url")
    private String cnicFrontUrl;
    @Column(name = "cnicBack_url")
    private String cnicBackUrl;
    @Column(name = "mac_address")
    private String macAddress;
    @Column(name = "monthly_amount")
    private double monthlyAmount;
    @Column(name = "is_paid")
    private boolean isPaid;
    private boolean deleted;
    @Column(name = "image_url")
    private String imageUrl;
    @Column(name = "expiry_date")
    private Date expiryDate;
    @Transient
    private MultipartFile cnicFront;
    @Transient
    private MultipartFile cnicBack;
    @Transient
    private String cincPath;
    @Transient
    private int locationId;
    @ManyToOne()
    @JoinColumn(name = "packageprice_Id", nullable = true)
    private PackagePrice packagePrices;
    @ManyToOne()
    @JoinColumn(name = "location_id")
    private Location location;
    @OneToMany(mappedBy = "employees", cascade = CascadeType.ALL)
    private List<Payment> payments;
    @OneToMany(mappedBy = "employees", cascade = CascadeType.ALL)
    private List<MonthlyPayment> monthlyPayments;

    public List<MonthlyPayment> getMonthlyPayments() {
        return monthlyPayments;
    }

    public void setMonthlyPayments(List<MonthlyPayment> monthlyPayments) {
        this.monthlyPayments = monthlyPayments;
    }

    public PackagePrice getPackagePrices() {
        return packagePrices;
    }

    public void setPackagePrices(PackagePrice packagePrices) {
        this.packagePrices = packagePrices;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCnic() {
        return cnic;
    }

    public void setCnic(String cnic) {
        this.cnic = cnic;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public String getCnicFrontUrl() {
        return cnicFrontUrl;
    }

    public void setCnicFrontUrl(String cnicFrontUrl) {
        this.cnicFrontUrl = cnicFrontUrl;
    }

    public String getCnicBackUrl() {
        return cnicBackUrl;
    }

    public void setCnicBackUrl(String cnicBackUrl) {
        this.cnicBackUrl = cnicBackUrl;
    }

    public MultipartFile getCnicFront() {
        return cnicFront;
    }

    public void setCnicFront(MultipartFile cnicFront) {
        this.cnicFront = cnicFront;
    }

    public MultipartFile getCnicBack() {
        return cnicBack;
    }

    public void setCnicBack(MultipartFile cnicBack) {
        this.cnicBack = cnicBack;
    }

    public String getCincPath() {
        return cincPath;
    }

    public void setCincPath(String cincPath) {
        this.cincPath = cincPath;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public double getMonthlyAmount() {
        return monthlyAmount;
    }

    public void setMonthlyAmount(double monthlyAmount) {
        this.monthlyAmount = monthlyAmount;
    }

    public boolean isPaid() {
        return isPaid;
    }

    public void setPaid(boolean paid) {
        isPaid = paid;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }
}
