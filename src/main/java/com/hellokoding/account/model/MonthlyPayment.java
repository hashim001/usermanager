/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hellokoding.account.model;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author muhammad
 */
@Entity
@Table(name = "monthly_payment")
public class MonthlyPayment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "monthlyPayment_id")
    private int monthlyPaymentId;
    @ManyToOne()
    @JoinColumn(name = "employee_id", nullable = true)
    private Employee employees;
    @ManyToOne()
    @JoinColumn(name = "paymentgenerate_Id", nullable = true)
    private PaymentGenerate paymentGenerates;
    @Column(name = "paymentRate")
    private double paymentRate;
    @Column(name = "date")
    private Date date;

    public int getMonthlyPaymentId() {
        return monthlyPaymentId;
    }

    public void setMonthlyPaymentId(int monthlyPaymentId) {
        this.monthlyPaymentId = monthlyPaymentId;
    }

    public Employee getEmployees() {
        return employees;
    }

    public void setEmployees(Employee employees) {
        this.employees = employees;
    }

    public PaymentGenerate getPaymentGenerates() {
        return paymentGenerates;
    }

    public void setPaymentGenerates(PaymentGenerate paymentGenerates) {
        this.paymentGenerates = paymentGenerates;
    }

    public double getPaymentRate() {
        return paymentRate;
    }

    public void setPaymentRate(double paymentRate) {
        this.paymentRate = paymentRate;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
