package com.hellokoding.account.model;

import javax.persistence.*;

@Entity
@Table(name = "user_auth")
public class UserAuth {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int auth_id;
    @Column(name="user_name")
    private String userName;
    @Column(name="email_id")
    private String emailId;
    private String password;
    @Transient
    private String passwordConfirm;
    @Column(name="isActive")
    private int isActive;
    @Column(name="user_type")
    private String userType;
    @Column(name="user_id")
    private String userId;

    public int getAuthId() {
        return auth_id;
    }

    public void setAuthId(int authId) {
        this.auth_id = authId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    @Transient
    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


}
