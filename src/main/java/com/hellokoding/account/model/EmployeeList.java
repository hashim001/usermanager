package com.hellokoding.account.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class EmployeeList implements Serializable {

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    private List<Employee> employees = new ArrayList<>();
    private List<PackagePrice> packageList = new ArrayList<>();

    public List<Employee> getEmployees() {
        return employees;
    }

    public List<PackagePrice> getPackageList() {
        return packageList;
    }

    public void setPackageList(List<PackagePrice> packageList) {
        this.packageList = packageList;
    }
}
