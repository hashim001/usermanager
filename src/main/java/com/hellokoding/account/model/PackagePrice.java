/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hellokoding.account.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author muhammad
 */
@Entity
@Table(name = "package_price")
public class PackagePrice implements Serializable {

    @Id
    @Column(name = "packageprice_id")
    private int packagePriceId;
    @Column(name = "package_name")
    private String packageName;
    @Column(name = "package_price")
    private double packagePrice;

    @OneToMany(mappedBy = "packagePrices", cascade = CascadeType.ALL)
    private List<Employee> employees;

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public int getPackagePriceId() {
        return packagePriceId;
    }

    public void setPackagePriceId(int packagePriceId) {
        this.packagePriceId = packagePriceId;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public double getPackagePrice() {
        return packagePrice;
    }

    public void setPackagePrice(double packagePrice) {
        this.packagePrice = packagePrice;
    }

}
