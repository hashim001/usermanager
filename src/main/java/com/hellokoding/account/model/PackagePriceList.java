/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hellokoding.account.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author muhammad
 */
public class PackagePriceList {

    private List<PackagePrice> packagePrices = new ArrayList<>();

    public List<PackagePrice> getPackagePrices() {
        return packagePrices;
    }

    public void setPackagePrices(List<PackagePrice> packagePrices) {
        this.packagePrices = packagePrices;
    }

}
