/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hellokoding.account.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author muhammad
 */
@Entity
@Table(name = "paymentgenerate")
public class PaymentGenerate implements Serializable {

    @Id
    @Column(name = "paymentgenerate_id")
    private int paymentGenerateId;
    @Column(name = "month")
    private String month;
    @Column(name = "year")
    private int year;
    @Column(name = "date")
    private String date;

    @OneToMany(mappedBy = "paymentGenerates", cascade = CascadeType.ALL)
    private List<MonthlyPayment> monthlyPayments;

    public List<MonthlyPayment> getMonthlyPayments() {
        return monthlyPayments;
    }

    public void setMonthlyPayments(List<MonthlyPayment> monthlyPayments) {
        this.monthlyPayments = monthlyPayments;
    }

    public int getPaymentGenerateId() {
        return paymentGenerateId;
    }

    public void setPaymentGenerateId(int paymentGenerateId) {
        this.paymentGenerateId = paymentGenerateId;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
