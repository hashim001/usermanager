/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hellokoding.account.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author muhammad
 */
public class PaymentList {

    private List<Payment> payments = new ArrayList<>();

    public List<Payment> getPayments() {
        return payments;
    }   

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

}
