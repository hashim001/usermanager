/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hellokoding.account.model;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.*;

/**
 *
 * @author muhammad
 */
@Entity
@Table(name = "payment")
public class Payment implements Serializable {

    @Id
    @Column(name = "payment_id")
    private int paymentId;
    @Column(name = "year")
    private int year;
    @Column(name = "month")
    private int month;
    @Column(name = "payment_date")
    private Date paymentDate;
    @Column(name = "payment_amount")
    private double paymentAmount;
    @Transient
    private int employeeId;
    @Transient
    private double previousAmount;

    @Transient
    private int previousEmployee;

    @ManyToOne()
    @JoinColumn(name = "employee_id", nullable = true)
    private Employee employees;

    public int getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }

    public Employee getEmployees() {
        return employees;
    }

    public void setEmployees(Employee employees) {
        this.employees = employees;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public double getPreviousAmount() {
        return previousAmount;
    }

    public void setPreviousAmount(double previousAmount) {
        this.previousAmount = previousAmount;
    }

    public int getPreviousEmployee() {
        return previousEmployee;
    }

    public void setPreviousEmployee(int previousEmployee) {
        this.previousEmployee = previousEmployee;
    }
}
