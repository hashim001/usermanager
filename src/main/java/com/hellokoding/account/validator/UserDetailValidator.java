package com.hellokoding.account.validator;

import com.hellokoding.account.model.Employee;
import com.hellokoding.account.service.user.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class UserDetailValidator implements Validator {

    @Autowired
    private RegistrationService registrationService;

    @Override
    public boolean supports(Class<?> aClass) {
        return Employee.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Employee user = (Employee) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty");
            if (user.getName().length() < 4 || user.getName().length() > 32) {
            errors.rejectValue("name", "Size.detailForm.name");
        }
        //check if cnic already exists. Employee check is to ignore cnic check in case of update
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cnic", "NotEmpty");
        if (registrationService.findByCnic(user.getCnic()) != null && registrationService.findByEmployeeId(user.getEmployeeId()) == null) {
            errors.rejectValue("cnic", "Duplicate.detailForm.cnic");
        }

        if (!(user.getCnic().length() >= 13 && user.getCnic().length() <= 16)) {
            errors.rejectValue("cnic", "Duplicate.detailForm.cnicFormat");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phone", "NotEmpty");
        if (user.getPhone().length() < 8 || user.getPhone().length() > 32) {
            errors.rejectValue("phone", "Size.detailForm.contact");
        }

    }
}
